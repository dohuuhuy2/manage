FROM node:16.17.0-alpine3.16

WORKDIR /manage

COPY package.json .

COPY yarn.lock .

RUN apk add --no-cache git openssh

RUN --mount=type=cache,target=/manage/yarn-cache \
    yarn install --pure-lockfile

COPY . .

RUN yarn build

CMD yarn start
