import styles from './styles.module.scss';

export type _ExpProps = {
  //
};

const _Exp: React.FC<_ExpProps> = (): JSX.Element => {
  return <div className={styles._Exp} />;
};

export default _Exp;
