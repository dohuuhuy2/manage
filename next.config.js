const withPugins = require('next-compose-plugins');
require('dotenv').config();
const ip = require('ip');
const moment = require('moment-timezone');
const running = `http://${ip.address()}:${process.env.PORT}`;
console.info('running :>> ', running);

const modifiedDate = moment()
  .tz('Asia/Ho_Chi_Minh')
  .format('DD/MM/YYYY HH:mm:ss');

const nextConfig = {
  reactStrictMode: false,
  publicRuntimeConfig: {
    modifiedDate,
  },
  experimental: {
    externalDir: true,
    forceSwcTransforms: true,
  },

  swcMinify: true,
  images: {
    domains: ['api.sshop.live'],
    disableStaticImages: true,
  },

  env: {
    IP_LAN: ip.address(),
    ENV: process.env.ENV,
    PORT: process.env.PORT,
  },
  webpack(config) {
    return config;
  },
};

const plugins = [];

module.exports = withPugins(plugins, nextConfig);
