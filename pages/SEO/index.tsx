import { SEOPage } from '@components/screens';
import { IPage } from '@process/define/page';
import { LayoutAdmin } from 'layouts';

const SEO: IPage = () => {
  return <SEOPage />;
};

SEO.layout = LayoutAdmin;
export default SEO;
