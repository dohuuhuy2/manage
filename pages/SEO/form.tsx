import { SEOFormPage } from '@components/screens';
import { IPage } from '@process/define/page';
import { LayoutAdmin } from 'layouts';

const SEOForm: IPage = () => {
  return <SEOFormPage />;
};

SEOForm.layout = LayoutAdmin;
export default SEOForm;
