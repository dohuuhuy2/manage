import { ImagePage } from '@components/screens';
import { IPage } from '@process/define/page';
import { actions } from '@store/reducer';
import { LayoutAdmin } from 'layouts';
import React from 'react';
import { useDispatch } from 'react-redux';

const PageImages: IPage = () => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(actions.requestImages());
  }, [dispatch]);

  return <ImagePage />;
};

PageImages.layout = LayoutAdmin;
export default PageImages;
