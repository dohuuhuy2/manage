import { HomePage } from '@components/screens';
import { IPage } from '@process/define/page';
import { LayoutAdmin } from 'layouts';

const Home: IPage = () => {
  return <HomePage />;
};

Home.layout = LayoutAdmin;
Home.requireAuth = true;
export default Home;
