import { LayoutAdmin } from 'layouts';
import { IPage } from '@process/define/page';
import { PartnersPage } from '@components/screens';

const Partners: IPage = () => {
  return <PartnersPage />;
};

Partners.layout = LayoutAdmin;
export default Partners;
