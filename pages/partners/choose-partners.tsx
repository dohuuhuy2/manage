import { ChoosePartnersPage } from '@components/screens';
import { AuthLayout } from 'layouts';
import { IPage } from '@process/define/page';

const ChoosePartners: IPage = () => {
  return <ChoosePartnersPage />;
};

ChoosePartners.layout = AuthLayout;
ChoosePartners.requireAuth = true;
export default ChoosePartners;
