import { LayoutAdmin } from 'layouts';
import { IPage } from '@process/define/page';
import { PartnerDetailsPage } from '@components/screens';

const PartnerDetails: IPage = () => {
  return <PartnerDetailsPage />;
};

PartnerDetails.layout = LayoutAdmin;
export default PartnerDetails;
