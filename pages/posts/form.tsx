import { PostDetailsPage } from '@components/screens';
import { LayoutAdmin } from 'layouts';
import { IPage } from '@process/define/page';

const PostDetails: IPage = () => {
  return <PostDetailsPage />;
};

PostDetails.layout = LayoutAdmin;
export default PostDetails;
