import { PostPage } from '@components/screens';
import { LayoutAdmin } from 'layouts';
import { IPage } from '@process/define/page';

const Posts: IPage = () => {
  return <PostPage />;
};

Posts.layout = LayoutAdmin;
export default Posts;
