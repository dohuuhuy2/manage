import { TagsPage } from '@components/screens';
import { LayoutAdmin } from 'layouts';
import { IPage } from '@process/define/page';

const Tags: IPage = () => {
  return <TagsPage />;
};

Tags.layout = LayoutAdmin;
export default Tags;
