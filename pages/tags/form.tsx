import { TagDetailsPage } from '@components/screens';
import { LayoutAdmin } from 'layouts';
import { IPage } from '@process/define/page';

const TagDetails: IPage = () => {
  return <TagDetailsPage />;
};

TagDetails.layout = LayoutAdmin;
export default TagDetails;
