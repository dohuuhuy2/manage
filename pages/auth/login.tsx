import { LoginPage } from '@components/screens';
import { AuthLayout } from 'layouts';
import { IPage } from '@process/define/page';

const Login: IPage = () => {
  return <LoginPage />;
};
Login.layout = AuthLayout;
export default Login;
