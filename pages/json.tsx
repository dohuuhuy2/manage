import { JsonPage } from '@components/screens';
import { IPage } from '@process/define/page';
import { actions } from '@store/reducer';
import { LayoutAdmin } from 'layouts';
import React from 'react';
import { useDispatch } from 'react-redux';

const PageJson: IPage = () => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(actions.requestJsons({}));
  }, [dispatch]);

  return <JsonPage />;
};
PageJson.layout = LayoutAdmin;
export default PageJson;
