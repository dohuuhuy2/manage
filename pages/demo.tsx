import { DemoPage } from '@components/screens';
import { IPage } from '@process/define/page';
import { Fragment } from 'react';

const Demo: IPage = () => {
  return <DemoPage />;
};

Demo.layout = Fragment;
export default Demo;
