import '@assets/styles/globals.scss';
import { Support } from '@components/cores';
import NoSsr from '@components/cores/NoSsr';
import ProtectPage from '@components/cores/ProtectPage';
import WrapperApp from '@components/cores/WrapperApp';
import axins from '@process/api';
import { IAppInit } from '@process/define/page';
import { persistor, wrapper } from '@store/init';
import NextNProgress from 'nextjs-progressbar';
import { Fragment } from 'react';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { PersistGate } from 'redux-persist/integration/react';

function MyApp({ Component, ...rest }: IAppInit) {
  const Layout = Component.layout ?? Fragment;

  const { store, props } = wrapper.useWrappedStore(rest);

  const { partner } = store.getState();

  if (partner.detail?.partnerId) {
    axins.defaults.headers.common.partnerId = partner.detail?.partnerId;
  }

  const site = (
    <>
      <NoSsr>
        <NextNProgress color="lightskyblue" height={3} />
        <WrapperApp>
          <Layout>
            <Component {...props.pageProps} />
          </Layout>
          <Support />
          <ToastContainer />
        </WrapperApp>
      </NoSsr>
    </>
  );

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {() => (
          <>
            {Component?.requireAuth ? <ProtectPage>{site}</ProtectPage> : site}
          </>
        )}
      </PersistGate>
    </Provider>
  );
}

export default MyApp;
