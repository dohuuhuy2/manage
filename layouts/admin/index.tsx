import Header from '@components/cores/Header';
import ProtectPage from '@components/cores/ProtectPage';
import SiderBar from '@components/cores/Siderbar';
import { ILayout } from '@process/define/page';
import styles from './styles.module.scss';

const LayoutAdmin = ({ children }: ILayout) => {
  return (
    <ProtectPage>
      <section id="LayoutHome" className={styles.LayoutAdmin}>
        <div className={styles.cover}>
          <Header />
          <main className={styles.containerHome}>
            <div className={styles.row}>
              <SiderBar />
              <div className={styles.colContain}>{children}</div>
            </div>
          </main>
        </div>
      </section>
    </ProtectPage>
  );
};

export default LayoutAdmin;
