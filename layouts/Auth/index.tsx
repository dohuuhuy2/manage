import { ILayout } from '@process/define/page';

import styles from './styles.module.scss';

const AuthLayout = ({ children }: ILayout) => {
  return (
    <section id="AuthLayout" className={styles.AuthLayout}>
      <main className={styles.main}>{children}</main>
    </section>
  );
};

export default AuthLayout;
