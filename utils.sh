print_hello() {
  echo "Hello, cưng !"
}

check_container_exist() {
  CONTAINER_NAME=$1
  if [ "$(docker inspect -f '{{.State.Running}}' $CONTAINER_NAME)" = "true" ]; then
    echo "Container $CONTAINER_NAME is running."
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME

  else
    echo "Container $CONTAINER_NAME is not running."
  fi
}


