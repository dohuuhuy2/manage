import { combineReducers } from '@reduxjs/toolkit';
import authorSlice from '@store/features/author/slice';
import imageSlice from '@store/features/image/slice';
import jsonSlice from '@store/features/json/slice';
import keywordSlice from '@store/features/keyword/slice';
import partnerSlice from '@store/features/partners/slice';
import postSlice from '@store/features/post/slice';
import SEOSlice from '@store/features/SEO/slice';
import tagSlice from '@store/features/tag/slice';
import totalSlice from '@store/features/total/slice';
import userSlice from '@store/features/user/slice';

export const actions = {
  ...userSlice.actions,
  ...totalSlice.actions,
  ...jsonSlice.actions,
  ...imageSlice.actions,
  ...authorSlice.actions,
  ...tagSlice.actions,
  ...partnerSlice.actions,
  ...keywordSlice.actions,
  ...postSlice.actions,
  ...SEOSlice.actions,
};

const sliceReducer = {
  [userSlice.name]: userSlice.reducer,
  [totalSlice.name]: totalSlice.reducer,
  [jsonSlice.name]: jsonSlice.reducer,
  [imageSlice.name]: imageSlice.reducer,
  [authorSlice.name]: authorSlice.reducer,
  [tagSlice.name]: tagSlice.reducer,
  [postSlice.name]: postSlice.reducer,
  [partnerSlice.name]: partnerSlice.reducer,
  [keywordSlice.name]: keywordSlice.reducer,
  [SEOSlice.name]: SEOSlice.reducer,
};

const rootReducer = combineReducers(sliceReducer);
export default rootReducer;
