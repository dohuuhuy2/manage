import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any,
  select: {} as any,
  detail: {
    partnerId: 'admin',
  } as any,
};

const partnerSlice = createSlice({
  name: 'partner',
  initialState,
  reducers: {
    requestPartners: (_state, _action) => undefined,
    savePartners: (
      state,
      { payload: { data } }: PayloadAction<{ data?: any }>,
    ) => {
      state.list = data;
    },
    selectPartner: (
      state,
      { payload: { item } }: PayloadAction<{ item?: any }>,
    ) => {
      state.select = item;
    },
    requestPartner: (_state, _action) => undefined,
    savePartner: (
      state,
      { payload: { data } }: PayloadAction<{ data?: any }>,
    ) => {
      state.detail = { ...data };
    },
  },
});

export default partnerSlice;
