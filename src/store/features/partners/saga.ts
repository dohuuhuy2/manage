import { urlPage } from '@@init/page';
import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import router from 'next/router';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestPartners({}: any) {
  try {
    const response: AxiosResponse = yield call(() =>
      axins.get('/partners/list', {
        headers: {
          author: 'manage',
        },
      }),
    );

    yield put(actions.savePartners({ data: response.data }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestPartners() {
  yield takeLatest(actions.requestPartners, requestPartners);
}

function* requestPartnerDetail({ payload }: any) {
  try {
    const { id } = payload;

    const response: AxiosResponse = yield call(() =>
      axins.get('partners/detail', {
        params: {
          _id: id,
        },
      }),
    );
    if (response.data) {
      yield put(actions.savePartner({ data: response.data }));

      router.push({
        pathname: urlPage.chiTietDoiTac,
      });
    }
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestPartnerDetail() {
  yield takeLatest(actions.requestPartner, requestPartnerDetail);
}

const partnerSagas = function* root() {
  yield all([fork(watcherRequestPartners), fork(watcherRequestPartnerDetail)]);
};

export { partnerSagas };
