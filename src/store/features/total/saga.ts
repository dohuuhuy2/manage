import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestMenuSiderbar() {
  try {
    const response: AxiosResponse = yield call(() =>
      axins.get('/json/query', {
        params: {
          partnerId: 'admin',
          folder: 'siderbar',
          name: 'menu',
        },
      }),
    );

    if (response.data) {
      yield put(actions.saveMenuSiderbar({ data: response.data }));
    }
  } catch (error) {
    console.error('error :>> ', error);
    console.info('error', error);
  }
}

function* watcherRequestMenuSiderbar() {
  yield takeLatest(actions.requestMenuSiderbar, requestMenuSiderbar);
}

const totalSagas = function* root() {
  yield all([fork(watcherRequestMenuSiderbar)]);
};
export { totalSagas };
