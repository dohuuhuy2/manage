export interface Loading {
  status: boolean;
  key: string;
  message?: string;
}
