import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { get } from 'lodash';
import { Loading } from './types';

const initialState = {
  status: '',
  siderbar: {} as any,
  partners: {
    list: [],
    select: {} as any,
  },
  loading: {} as any,
};

const totalSlice = createSlice({
  name: 'total',
  initialState,
  reducers: {
    requestMenuSiderbar: () => {
      //
    },
    saveMenuSiderbar: (state, action) => {
      state.siderbar = action.payload.data;
    },
    demoRequest: (state) => {
      state.status = 'loading';
      // action
    },
    requestListPartner: () => {
      //
    },
    savePartners: (state, action) => {
      state.partners.list = action.payload.data;
    },
    selectPartner: (state, action) => {
      state.partners.select = { ...action.payload.item };
    },
    toggleLoading: (state, action: PayloadAction<Loading>) => {
      const { status, key } = action.payload;
      const message = get(
        action.payload,
        'message',
        'Tiến trình đang thực hiện ...',
      );

      state.loading = {
        [key]: {
          status,
          message,
        },
        ...state.loading,
      };
    },
  },
});

export default totalSlice;
