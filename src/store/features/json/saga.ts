import axins from '@process/api';
import { AppState } from '@store/init';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';

function* requestJson() {
  try {
    const partner: AppState['partner'] = yield select((state) => state.partner);

    const response: AxiosResponse = yield call(() =>
      axins.get('/json/find', {
        params: {
          partnerId: partner.detail?.partnerId,
        },
      }),
    );

    yield put(actions.saveJsons({ data: response.data }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestJson() {
  yield takeLatest(actions.requestJsons, requestJson);
}

const jsonSagas = function* root() {
  yield all([fork(watcherRequestJson)]);
};

export { jsonSagas };
