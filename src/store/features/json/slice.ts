import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any[],
  select: {} as any,
};

const jsonSlice = createSlice({
  name: 'json',
  initialState,
  reducers: {
    requestJsons: (_state, _action) => undefined,
    saveJsons: (state, action) => {
      state.list = action.payload.data;
    },
    selectJson: (state, action) => {
      state.select = action.payload.item;
    },
  },
});

export default jsonSlice;
