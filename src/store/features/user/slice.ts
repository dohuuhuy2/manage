import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  info: {} as any,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setInfo: (state, action) => {
      state.info = action.payload.data;
    },
  },
});

export default userSlice;
