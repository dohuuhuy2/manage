import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any,
  select: {} as any,
  detail: {} as any,
};

const SEOSlice = createSlice({
  name: 'SEO',
  initialState,
  reducers: {
    getListSEO: (_state, _action) => undefined,
    saveListSEO: (state, action: PayloadAction<{ data?: any }>) => {
      state.list = action.payload.data;
    },
    selectSEO: (state, action: PayloadAction<{ item?: any }>) => {
      state.select = action.payload.item;
    },
    getSEODetail: (_state, _action) => undefined,
    saveSEODetail: (state, action: PayloadAction<{ data?: any }>) => {
      state.detail = action.payload.data;
    },
  },
});

export default SEOSlice;
