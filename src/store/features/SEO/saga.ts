import { urlPage } from '@@init/page';
import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import router from 'next/router';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestSEO({}: any) {
  try {
    const response: AxiosResponse = yield call(() =>
      axins.get('SEO/list', {
        headers: {},
      }),
    );

    yield put(actions.saveListSEO({ data: response.data }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestSEO() {
  yield takeLatest(actions.getListSEO, requestSEO);
}

function* requestSEODetail({ payload }: any) {
  try {
    const { key } = payload;

    const response: AxiosResponse = yield call(() =>
      axins.get('SEO/detail', {
        params: { key },
      }),
    );
    if (response.data) {
      yield put(actions.saveSEODetail({ data: response.data }));

      router.push(
        {
          pathname: urlPage.chiTietSEO,
          query: {
            key,
          },
        },
        undefined,
      );
    }
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestSEODetail() {
  yield takeLatest(actions.getSEODetail, requestSEODetail);
}

const SEOSagas = function* root() {
  yield all([fork(watcherRequestSEO), fork(watcherRequestSEODetail)]);
};

export { SEOSagas };
