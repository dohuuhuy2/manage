import axins from '@process/api';
import { AppState } from '@store/init';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import { all, call, fork, put, select, takeLatest } from 'redux-saga/effects';

function* requestImage() {
  try {
    const partner: AppState['partner'] = yield select((state) => state.partner);

    const response: AxiosResponse = yield call(() =>
      axins.get('images/list', {
        params: {
          partnerId: partner.detail?.partnerId,
        },
        headers: {
          author: 'manage',
        },
      }),
    );

    yield put(actions.saveImages({ data: response.data }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestImage() {
  yield takeLatest(actions.requestImages, requestImage);
}

const imageSagas = function* root() {
  yield all([fork(watcherRequestImage)]);
};
export { imageSagas };
