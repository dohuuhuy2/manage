import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any[],
  select: {} as any,
};

const imageSlice = createSlice({
  name: 'image',
  initialState,
  reducers: {
    requestImages: () => undefined,
    saveImages: (state, action) => {
      state.list = action.payload.data;
    },
    selectImage: (state, action) => {
      state.select = action.payload.item;
    },
  },
});

export default imageSlice;
