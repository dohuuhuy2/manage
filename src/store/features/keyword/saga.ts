import { urlPage } from '@@init/page';
import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import router from 'next/router';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestKeywords({}: any) {
  try {
    const response: AxiosResponse = yield call(() =>
      axins.get('keywords/list', {
        headers: {
          keyword: 'manage',
        },
      }),
    );

    yield put(actions.saveKeywords({ data: response.data }));

    yield put(actions.toggleLoading({ status: false, key: 'listKeywords' }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestKeywords() {
  yield takeLatest(actions.requestKeywords, requestKeywords);
}

function* requestKeywordDetail({ payload }: any) {
  try {
    const { slug } = payload;

    const response: AxiosResponse = yield call(() =>
      axins.get('keywords/detail', {
        params: { slug },
      }),
    );
    if (response.data) {
      yield put(actions.saveKeyword({ data: response.data }));

      router.push({
        pathname: urlPage.chiTietKeywords,
        query: {
          slug,
        },
      });
    }
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestKeywordDetail() {
  yield takeLatest(actions.requestKeyword, requestKeywordDetail);
}

const keywordSagas = function* root() {
  yield all([fork(watcherRequestKeywords), fork(watcherRequestKeywordDetail)]);
};

export { keywordSagas };
