import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any,
  select: {} as any,
  detail: {} as any,
};

const keywordSlice = createSlice({
  name: 'keyword',
  initialState,
  reducers: {
    requestKeywords: (_state, _action) => undefined,
    saveKeywords: (state, action: PayloadAction<{ data?: any }>) => {
      state.list = action.payload.data;
    },
    selectKeyword: (state, action: PayloadAction<{ item?: any }>) => {
      state.select = action.payload.item;
    },
    requestKeyword: (_state, _action) => undefined,
    saveKeyword: (state, action: PayloadAction<{ data?: any }>) => {
      state.detail = action.payload.data;
    },
  },
});

export default keywordSlice;
