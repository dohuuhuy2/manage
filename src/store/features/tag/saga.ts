import { urlPage } from '@@init/page';
import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import router from 'next/router';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestTags({}: any) {
  try {
    yield put(actions.toggleLoading({ status: true, key: 'listTags' }));

    const response: AxiosResponse = yield call(() =>
      axins.get('tags/list', {
        headers: {
          author: 'manage',
        },
      }),
    );

    yield put(actions.saveTags({ data: response.data }));

    yield put(actions.toggleLoading({ status: false, key: 'listTags' }));
  } catch (error) {
    console.error('error :>> ', error);
    yield put(actions.toggleLoading({ status: false, key: 'listTags' }));
  }
}

function* watcherRequestTags() {
  yield takeLatest(actions.requestTags, requestTags);
}

function* requestTagDetail({ payload }: any) {
  try {
    const { slug } = payload;

    yield put(actions.toggleLoading({ status: true, key: 'tagDetail' }));

    const response: AxiosResponse = yield call(() =>
      axins.get('tags/detail', {
        params: { slug },
      }),
    );
    if (response.data) {
      yield put(actions.saveTag({ data: response.data }));

      router.push(
        {
          pathname: urlPage.chiTietDanhMuc,
          query: {
            slug,
          },
        },
        undefined,
      );
    }

    yield put(actions.toggleLoading({ status: false, key: 'tagDetail' }));
  } catch (error) {
    console.error('error :>> ', error);
    yield put(actions.toggleLoading({ status: false, key: 'tagDetail' }));
  }
}

function* watcherRequestTagDetail() {
  yield takeLatest(actions.requestTag, requestTagDetail);
}

const tagSagas = function* root() {
  yield all([fork(watcherRequestTags), fork(watcherRequestTagDetail)]);
};

export { tagSagas };
