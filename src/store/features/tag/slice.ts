import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any,
  select: {} as any,
  detail: {} as any,
};

const tagSlice = createSlice({
  name: 'tag',
  initialState,
  reducers: {
    requestTags: (_state, _action) => undefined,
    saveTags: (state, action: PayloadAction<{ data?: any }>) => {
      state.list = action.payload.data;
    },
    selectTag: (state, action: PayloadAction<{ item?: any }>) => {
      state.select = action.payload.item;
    },
    requestTag: (_state, _action) => undefined,
    saveTag: (state, action: PayloadAction<{ data?: any }>) => {
      state.detail = action.payload.data;
    },
  },
});

export default tagSlice;
