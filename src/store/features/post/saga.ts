import { urlPage } from '@@init/page';
import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import router from 'next/router';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestPosts({}: any) {
  try {
    const response: AxiosResponse = yield call(() =>
      axins.get('posts/list', {
        headers: {
          author: 'manage',
        },
        params: { limit: 100 },
      }),
    );

    yield put(actions.saveListPost({ data: response.data }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestPosts() {
  yield takeLatest(actions.requestPosts, requestPosts);
}

function* requestPostDetail({ payload }: any) {
  try {
    const { slug } = payload;

    const response: AxiosResponse = yield call(() =>
      axins.get('posts/detail', {
        params: { slug },
      }),
    );
    if (response.data) {
      yield put(actions.savePostDetail({ data: response.data }));

      router.push(
        {
          pathname: urlPage.chiTietBaiViet,
          query: {
            slug,
          },
        },
        undefined,
      );
    }
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestPostDetail() {
  yield takeLatest(actions.getPostDetail, requestPostDetail);
}

const postSagas = function* root() {
  yield all([fork(watcherRequestPosts), fork(watcherRequestPostDetail)]);
};

export { postSagas };
