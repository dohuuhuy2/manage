import { ListPostType, PostType } from '@process/@type';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  list: {} as ListPostType,
  select: {} as PostType,
  detail: {} as PostType,
};

const postSlice = createSlice({
  name: 'post',
  initialState,
  reducers: {
    requestPosts: (_state, _action) => undefined,
    saveListPost: (state, action: PayloadAction<{ data?: any }>) => {
      state.list = action.payload.data;
    },
    selectPost: (state, action: PayloadAction<{ item?: any }>) => {
      state.select = action.payload.item;
    },
    getPostDetail: (_state, _action) => undefined,
    savePostDetail: (state, action: PayloadAction<{ data?: any }>) => {
      state.detail = action.payload.data;
    },
  },
});

export default postSlice;
