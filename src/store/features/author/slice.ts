import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState = {
  list: [] as any,
  select: {} as any,
  detail: {} as any,
};

const authorSlice = createSlice({
  name: 'author',
  initialState,
  reducers: {
    requestAuthors: (_state, _action) => undefined,
    saveAuthors: (state, action: PayloadAction<{ data?: any }>) => {
      state.list = action.payload.data;
    },
    selectAuthor: (state, action: PayloadAction<{ item?: any }>) => {
      state.select = action.payload.item;
    },
    requestAuthor: (_state, _action) => undefined,
    saveAuthor: (state, action: PayloadAction<{ data?: any }>) => {
      state.detail = action.payload.data;
    },
  },
});

export default authorSlice;
