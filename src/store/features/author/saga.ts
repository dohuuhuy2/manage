import { urlPage } from '@@init/page';
import axins from '@process/api';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import router from 'next/router';
import { all, call, fork, put, takeLatest } from 'redux-saga/effects';

function* requestAuthors({}: any) {
  try {
    const response: AxiosResponse = yield call(() =>
      axins.get('authors/list', {
        headers: {
          author: 'manage',
        },
      }),
    );

    yield put(actions.saveAuthors({ data: response.data }));
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestAuthors() {
  yield takeLatest(actions.requestAuthors, requestAuthors);
}

function* requestAuthorDetail({ payload }: any) {
  try {
    const { slug } = payload;

    const response: AxiosResponse = yield call(() =>
      axins.get('authors/detail', {
        params: { slug },
      }),
    );
    if (response.data) {
      yield put(actions.saveAuthor({ data: response.data }));

      router.push(
        {
          pathname: urlPage.chiTietTacGia,
          query: {
            slug,
          },
        },
        undefined,
      );
    }
  } catch (error) {
    console.error('error :>> ', error);
  }
}

function* watcherRequestAuthorDetail() {
  yield takeLatest(actions.requestAuthor, requestAuthorDetail);
}

const authorSagas = function* root() {
  yield all([fork(watcherRequestAuthors), fork(watcherRequestAuthorDetail)]);
};

export { authorSagas };
