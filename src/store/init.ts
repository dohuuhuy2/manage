import mainConfig from '@process/configs/main-config';
import { configureStore } from '@reduxjs/toolkit';
import { createWrapper } from 'next-redux-wrapper';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducer';
import rootSaga from './saga';

const sagaMiddleware = createSagaMiddleware();

const rootPersist = persistReducer(mainConfig.reduxPersist, rootReducer);

const store = configureStore({
  reducer: rootPersist,
  middleware: [sagaMiddleware],
  devTools: mainConfig.isDevEnv,
});

const stores = () => {
  sagaMiddleware.run(rootSaga);
  return store;
};

export const persistor = persistStore(store);

export type ReduxStore = ReturnType<typeof stores>;
export type AppState = ReturnType<typeof store.getState>;
export type Dispatch = typeof store.dispatch;

export const useAppDispatch: () => Dispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;

export const wrapper = createWrapper<ReduxStore>(stores);
