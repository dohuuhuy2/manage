import { authorSagas } from '@store/features/author/saga';
import { imageSagas } from '@store/features/image/saga';
import { jsonSagas } from '@store/features/json/saga';
import { keywordSagas } from '@store/features/keyword/saga';
import { partnerSagas } from '@store/features/partners/saga';
import { postSagas } from '@store/features/post/saga';
import { SEOSagas } from '@store/features/SEO/saga';
import { tagSagas } from '@store/features/tag/saga';
import { totalSagas } from '@store/features/total/saga';
import { all, fork } from 'redux-saga/effects';

export default function* rootSaga(): Generator {
  yield all([
    fork(totalSagas),
    fork(jsonSagas),
    fork(imageSagas),
    fork(tagSagas),
    fork(postSagas),
    fork(partnerSagas),
    fork(authorSagas),
    fork(keywordSagas),
    fork(SEOSagas),
  ]);
}
