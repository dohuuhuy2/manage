import DeleteIcon from '@assets/icons/DeleteIcon';
import LoadingIcon from '@assets/icons/LoadingIcon';
import UpImgIcon from '@assets/icons/UpImgIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import React from 'react';
import styles from './styles.module.scss';
import useUpload from './useUpload';
import moment from 'moment';

export type UploadImageProps = {
  aspectRatio?: any;
  preCondition: {
    partnerId: string;
    folder: string;
  };
  onChange?: (data: any) => void;
  onReset?: () => void;
  name?: string;
  label?: string;
  value?: any;
};

const UploadImage = ({
  preCondition,
  onChange,
  label,
  name: name_input,
  value,
}: UploadImageProps): JSX.Element => {
  const name = name_input || label || 'picture';

  const {
    error,
    loading,
    imgRef,
    viewImage,
    onChangeImg,
    onDeleteImg,
    handleDrop,
    handleDrag,
    onError,
  } = useUpload({
    preCondition,
    name,
    onChange,
    value,
  });

  return (
    <>
      <div className={styles.upload}>
        <input
          ref={imgRef}
          name={name}
          id={name}
          accept="image/*"
          type="file"
          onChange={onChangeImg}
          className={styles.inputFile}
        />
        <label id={name} htmlFor={name} className={styles.card}>
          <figure
            className={styles.view}
            onDragEnter={handleDrag}
            onDragLeave={handleDrag}
            onDragOver={handleDrag}
            onDrop={handleDrop}>
            {loading ? (
              <BoxIcon setting={{ size: 65 }} className={styles.iconLoading}>
                <LoadingIcon />
              </BoxIcon>
            ) : (
              <>
                {viewImage?.image_url ? (
                  <img
                    alt=""
                    src={`${viewImage?.image_url}?t=${moment().unix()}`}
                    onError={onError}
                  />
                ) : (
                  <div className={styles.note}>
                    <BoxIcon setting={{ size: 65 }}>
                      <UpImgIcon />
                    </BoxIcon>
                    <span className={styles.title}>Thả hoặc Chọn tệp</span>
                    <span className={styles.error}>{error}</span>
                  </div>
                )}
              </>
            )}
          </figure>
          <MDButton
            hug
            hidden={!viewImage?.image_url}
            background="transparent"
            onClick={onDeleteImg}
            className={styles.btnDelete}>
            <BoxIcon>
              <DeleteIcon fill="white" />
            </BoxIcon>
          </MDButton>
        </label>
      </div>
    </>
  );
};

export default React.memo(UploadImage);
