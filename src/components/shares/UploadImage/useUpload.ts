import axins from '@process/api';
import { AxiosResponse } from 'axios';
import { useCallback, useEffect, useRef, useState } from 'react';

export type UploadProps = {
  preCondition?: any;
  onChange?: (data: any) => void;
  name: string;
  value?: string;
};

const useUpload = ({ onChange, name, preCondition, value }: UploadProps) => {
  // -------------------------------------
  const imgRef = useRef<HTMLInputElement>(null);
  const [loading, setLoading] = useState(false);
  const [viewImage, setViewImage] = useState<any>(null);
  const [error, setError] = useState('');

  // -------------------------------------

  const onClearFile = useCallback(() => {
    onChange?.({ [name]: null });
    if (imgRef.current) {
      imgRef.current.value = '';
    }
  }, [name, onChange]);

  useEffect(() => {
    if (value) {
      setViewImage((prev: any) => ({
        image_url: value,
        ...prev,
      }));
    }
  }, [value]);

  useEffect(() => {
    if (process.browser) {
      window.onbeforeunload = async (e) => {
        e.preventDefault();
        await axins.delete(`images/remove/${viewImage?._id}`);
        e.returnValue = '';
        return '';
      };
    }
  }, [viewImage?._id]);

  const onDeleteImg = async () => {
    setLoading(true);
    try {
      onClearFile();
      if (viewImage?._id) {
        await axins.delete(`images/remove/${viewImage?._id}`);
      }
    } catch (err) {
      console.info('err :>> ', err);
    }
    setTimeout(() => {
      setViewImage({ image_url: '' });
      setLoading(false);
    }, 1000);
  };

  const onChangeImg = async (event: any) => {
    setLoading(true);
    event.preventDefault();
    event.stopPropagation();

    const { name, files } = event.target;

    if (files[0]) {
      const image_info = await onSaveImg({ file: files[0] });
      setViewImage(image_info);
      onChange?.({ [name]: image_info });
    }

    setLoading(false);
  };

  const onSaveImg = async ({ file }: any) => {
    try {
      const formData = new FormData();
      formData.append('partnerId', preCondition?.partnerId);
      formData.append('folder', preCondition?.folder);
      formData.append('platform', preCondition?.platform);
      formData.append('name', file?.name.split('.')[0]);
      formData.append('file', file);

      const reponse: AxiosResponse<any> = await axins.post(
        '/images/upload',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        },
      );

      return reponse.data;
    } catch (err) {
      console.info('err', err);

      return null;
    }
  };

  const handleDrop = async (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    const files = event.dataTransfer.files;

    if (files) {
      const image_info = await onSaveImg({ file: files[0] });

      setViewImage(image_info);
      onChange?.({ [name]: image_info });
    }
  };

  const handleDrag = (event: any) => {
    event?.preventDefault();
    event?.stopPropagation();
  };

  const onError = () => {
    setViewImage({});
    setError('Đường dẫn không tồn tại !');
  };

  return {
    error,
    loading,
    imgRef,
    viewImage,
    onDeleteImg,
    onChangeImg,
    handleDrop,
    handleDrag,
    onError,
  };
};

export default useUpload;
