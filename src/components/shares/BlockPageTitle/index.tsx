import AddIcon from '@assets/icons/AddIcon';
import RefreshIcon from '@assets/icons/RefreshIcon';
import TitleIcon from '@assets/icons/TitleIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import cx from 'classnames';
import { useState } from 'react';
import styles from './styles.module.scss';

export type BlockPageTitleProps = {
  onRefresh?: any;
  onCreate: any;
  title: string;
};

const BlockPageTitle: React.FC<BlockPageTitleProps> = ({
  onRefresh = undefined,
  onCreate = undefined,
  title,
}): JSX.Element => {
  const [isRefresh, setisRefresh] = useState(false);

  const handleRefresh = () => {
    onRefresh();
    setisRefresh(true);
    setTimeout(() => {
      setisRefresh(false);
    }, 2000);
  };

  return (
    <div className={styles.BlockPageTitle}>
      <p className={styles.title}>
        <BoxIcon setting={{ size: 45 }} className={styles.icon}>
          <TitleIcon />
        </BoxIcon>
        {title}
      </p>

      <div className={styles.groupAction}>
        <MDButton
          className={styles.btnRefresh}
          hidden={!onRefresh}
          onClick={handleRefresh}
          reasonable
          shadown={false}
          isBorder
          iconLeft={
            <BoxIcon
              className={cx(styles.icon, { [styles.isRefresh]: isRefresh })}>
              <RefreshIcon />
            </BoxIcon>
          }>
          refresh
        </MDButton>
        <MDButton
          hidden={!onCreate}
          onClick={onCreate}
          className={styles.btn}
          iconLeft={
            <BoxIcon setting={{ size: 28 }}>
              <AddIcon />
            </BoxIcon>
          }>
          Tạo mới
        </MDButton>
      </div>
    </div>
  );
};

export default BlockPageTitle;
