import CloseIcon from '@assets/icons/CloseIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import styles from './styles.module.scss';

export type ModalHeaderProps = {
  onClose: () => void;
  title: string;
};

const ModalHeader: React.FC<ModalHeaderProps> = ({
  onClose,
  title,
}): JSX.Element => {
  return (
    <div className={styles.ModalHeader}>
      <p className={styles.title}>{title}</p>
      <MDButton shadown={false} onClick={onClose} hug>
        <BoxIcon setting={{ size: 24 }}>
          <CloseIcon />
        </BoxIcon>
      </MDButton>
    </div>
  );
};

export default ModalHeader;
