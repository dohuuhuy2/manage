import AddIcon from '@assets/icons/AddIcon';
import BackIcon from '@assets/icons/BackIcon';
import FormIcon from '@assets/icons/FormIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import cx from 'classnames';
import { isEqual } from 'lodash';
import styles from './styles.module.scss';

type BlockFormTitleProps = {
  title: any;
  state: any;
  disabledState: any;
  onBack: any;
  onToggleState: any;
  onSave: any;
  onCreate?: any;
};

const BlockFormTitle: React.FC<BlockFormTitleProps> = ({
  title,
  state,
  disabledState,
  onBack,
  onToggleState,
  onSave,
  onCreate,
}): JSX.Element => {
  return (
    <div className={styles.BlockFormTitle}>
      <div className={styles.groupTitle}>
        <span className={styles.icon}>
          <FormIcon width={45} />
        </span>
        <p className={styles.title}>{title}</p>
      </div>

      <div className={styles.groupBtn}>
        <MDButton
          isBorder={false}
          onClick={onBack}
          className={cx(styles.btn, styles.btnBack)}>
          <BackIcon width={35} height={35} />
        </MDButton>

        <MDButton
          isBorder={false}
          onClick={onCreate}
          className={cx(styles.btn, styles.btnCreate)}
          iconLeft={
            <BoxIcon setting={{ size: 28 }}>
              <AddIcon />
            </BoxIcon>
          }
          background="lightblue"
          value={'New'}
        />

        <MDButton
          disabled={disabledState}
          value={`${isEqual(state, 'publish') ? 'Un publish' : 'Publish'}`}
          onClick={onToggleState}
          className={cx(styles.btn, styles.btnState, styles[state])}
        />

        <MDButton
          value={'save'}
          background="lightblue"
          onClick={onSave}
          className={styles.btn}
        />
      </div>
    </div>
  );
};

export default BlockFormTitle;
