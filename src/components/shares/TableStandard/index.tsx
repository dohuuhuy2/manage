import DeleteIcon from '@assets/icons/DeleteIcon';
import EditIcon from '@assets/icons/EditIcon';
import { BoxIcon, MDWrap } from '@components/libraries';
import cx from 'classnames';
import { isArray, isEqual, uniqueId } from 'lodash';
import styles from './styles.module.scss';

export type TableStandardProps = {
  className?: string;
  columns: {
    name: string;
    uid: string;
  }[];
  list: any[];
  onEdit: (item: any) => void;
  onDetele: (item: any) => void;
};

const TableStandard: React.FC<TableStandardProps> = ({
  className,
  columns = [],
  list = [],
  onEdit,
  onDetele,
}): JSX.Element => {
  return (
    <div className={cx(styles.TableStandard, className)}>
      <div>
        <span> tìm kiếm</span>
      </div>

      <table className={styles.table}>
        <thead className={styles.thead} role="row">
          <tr className={styles.row}>
            {isArray(columns) &&
              columns.map((ele, key) => {
                return (
                  <td key={key} className={styles.cell}>
                    {ele?.name}
                  </td>
                );
              })}
          </tr>
        </thead>
        <tbody className={styles.tbody}>
          {isArray(list) &&
            list?.map((ele: any, key = 0) => {
              return (
                <tr
                  key={key}
                  className={styles.row}
                  onDoubleClick={() => onEdit(ele)}>
                  {isArray(columns) &&
                    columns.map((item) => {
                      if (isEqual(item?.uid, 'actions')) {
                        return (
                          <td className={styles.cell} key={uniqueId()}>
                            <MDWrap className={styles.groupIcon}>
                              <BoxIcon
                                setting={{ size: 24 }}
                                className={styles.icon}
                                onClick={() => onEdit(ele)}>
                                <EditIcon />
                              </BoxIcon>

                              <BoxIcon
                                setting={{ size: 24 }}
                                className={cx(styles.icon, styles.iconDel)}
                                onClick={() => onDetele(ele)}>
                                <DeleteIcon />
                              </BoxIcon>
                            </MDWrap>
                          </td>
                        );
                      }

                      return (
                        <td className={cx(styles.cell)} key={uniqueId()}>
                          <span
                            className={cx(
                              styles.value,
                              styles[ele?.[item?.uid]],
                            )}>
                            {isEqual(item?.uid, 'id')
                              ? ++key
                              : ele?.[item?.uid] || null}
                          </span>
                        </td>
                      );
                    })}
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
};

export default TableStandard;
