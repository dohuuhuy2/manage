import { MDButton, MDWrap } from '@components/libraries';
import styles from './styles.module.scss';

export type Block = {
  turn?: boolean;
  value: string | JSX.Element;
  action?: any;
};

export type ModalGroupActionProps = {
  del?: Block;
  oke?: Block;
  close?: Block;
};

const ModalGroupAction: React.FC<ModalGroupActionProps> = ({
  del,
  oke,
  close,
}): JSX.Element => {
  return (
    <>
      <MDWrap right wrap className={styles.ModalGroupAction}>
        {close?.turn && (
          <MDButton
            onClick={close?.action}
            reasonable
            shadown={false}
            className={styles.btnClose}>
            {close?.value}
          </MDButton>
        )}

        {del?.turn && (
          <MDButton
            shadown={false}
            reasonable
            onClick={del?.action}
            className={styles.btnDelele}>
            {del?.value}
          </MDButton>
        )}

        {oke?.turn && (
          <MDButton reasonable onClick={oke?.action} className={styles.btnOke}>
            {oke?.value}
          </MDButton>
        )}
      </MDWrap>
    </>
  );
};

export default ModalGroupAction;
