import EditIcon from '@assets/icons/EditIcon';
import GalleryTickIcon from '@assets/icons/GalleryTickIcon';
import TickIcon from '@assets/icons/TickIcon';
import UploadIcon from '@assets/icons/UploadIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import axins from '@process/api';
import { toBlob } from '@process/utils/func';
import { AxiosResponse } from 'axios';
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import ModalCrop from './ModalCrop';
import styles from './styles.module.scss';

export type UploadImagesProps = {
  srcImg: any;
  aspectRatio?: any;
  prerequisites: {
    partnerId: string;
    folder: string;
    platform: string;
  };
  onChange?: (data: any) => void;
  name?: string;
  label?: string;
};

const UploadImages: React.FC<UploadImagesProps> = ({
  srcImg = null,
  prerequisites,
  onChange,
  label = '',
  name = label,
  aspectRatio,
}): JSX.Element => {
  const [urlImg, setUrlImg] = useState<any>({ [name]: srcImg });
  const [originImg, setOriginImg] = useState<any>({ [name]: '' });
  const [showModal, setshowModal] = useState(false);
  const [isDone, setIsDone] = useState(false);

  const onToggleShowModal = () => {
    if (urlImg) {
      setshowModal(!showModal);
    } else {
      toast.error('Vui lòng kéo hoặc chọn tệp hình ảnh !');
    }
  };

  const onSetOriginImg = (data: any) => {
    setOriginImg((prev: any) => ({ ...prev, [name]: data }));
  };

  const onSetUrlImg = (data: any) => {
    setUrlImg((prev: any) => ({ ...prev, [name]: data }));
  };

  const onChangeImg = (event: any) => {
    event.preventDefault();
    let files: any;
    if (event.dataTransfventr) {
      files = event.dataTransfer.files;
    } else if (event.target) {
      files = event.target.files;
    }
    onSetOriginImg(files[0]);
    const reader = new FileReader();
    reader.onload = () => {
      onSetUrlImg(reader.result);
    };
    files[0] && reader?.readAsDataURL(files[0]);
  };

  const onSaveImg = async () => {
    try {
      if (urlImg[name]) {
        const formData = new FormData();
        formData.append('partnerId', prerequisites?.partnerId);
        formData.append('folder', prerequisites?.folder);
        formData.append(
          'name',
          `${prerequisites?.folder}_${name}_${originImg[name]?.name}`,
        );
        formData.append('platform', prerequisites?.platform);
        const blobImg = await toBlob(urlImg[name]);
        formData.append('file', blobImg, originImg[name]?.name);

        const reponse: AxiosResponse<any> = await axins.post(
          '/images/upload',
          formData,
          {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          },
        );
        if (reponse.data) {
          toast.success('Lưu thành công !');
          setIsDone(true);
          onChange?.(reponse.data.data);
        }
      }
    } catch (err) {
      console.info('err', err);
    }
  };

  return (
    <>
      <div className={styles.UploadImages}>
        <label htmlFor={name} className={styles.wrapper}>
          <div className={styles.groupNote}>
            <div className={styles.note}>
              <>
                <BoxIcon
                  setting={{ size: 48, stroke: '#27d4ce' }}
                  className={styles.updloadIcon}>
                  <UploadIcon />
                </BoxIcon>
              </>
              <div>
                <p className={styles.title}>Thả hoặc Chọn tệp</p>
                <p className={styles.guide}>
                  Thả tệp vào đây hoặc nhấp vào duyệt qua máy của bạn
                </p>
              </div>
            </div>
            <div className={styles.groupAction}>
              <MDButton
                reasonable
                onClick={onSaveImg}
                hidden={!urlImg[name]}
                color="#3498db"
                className={styles.btnSave}
                iconLeft={
                  <BoxIcon setting={{ stroke: '#3498db' }}>
                    <GalleryTickIcon />
                  </BoxIcon>
                }>
                Save
              </MDButton>

              <MDButton
                hidden={!urlImg[name]}
                onClick={onToggleShowModal}
                reasonable
                shadown={false}
                className={styles.btnEdit}
                isBorder
                iconLeft={
                  <BoxIcon setting={{ stroke: '#07bc0c' }}>
                    <EditIcon />
                  </BoxIcon>
                }>
                Crop
              </MDButton>
            </div>
          </div>

          <figure className={styles.view}>
            <img alt="" src={urlImg[name]} />
            {isDone && (
              <BoxIcon
                setting={{ size: 48, stroke: 'lightgreen' }}
                className={styles.tickIcon}>
                <TickIcon />
              </BoxIcon>
            )}
          </figure>
        </label>
        <input
          name={name}
          id={name}
          accept="image/*"
          type="file"
          hidden
          onChange={onChangeImg}
        />
      </div>

      <ModalCrop
        {...{
          name,
          aspectRatio,
          open: showModal,
          onToggle: onToggleShowModal,
          urlImg: urlImg[name],
          onSetUrlImg,
        }}
      />
    </>
  );
};

export default React.memo(UploadImages);
