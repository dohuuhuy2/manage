import { MDButton, MDModal } from '@components/libraries';
import ModalGroupAction from '@components/shares/ModalGroupAction';
import ModalHeader from '@components/shares/ModalHeader';
import { createRef, useState } from 'react';
import { Cropper, ReactCropperElement } from 'react-cropper';
import 'react-image-crop/dist/ReactCrop.css';
import styles from './styles.module.scss';

export type ModalCropProps = {
  name: string;
  open: boolean;
  onToggle: () => void;
  urlImg?: any;
  aspectRatio?: any;
  onSetUrlImg: any;
};

const ModalCrop: React.FC<ModalCropProps> = ({
  open,
  onToggle,
  urlImg,
  aspectRatio = 16 / 9,
  onSetUrlImg,
}): JSX.Element => {
  const cropperRef = createRef<ReactCropperElement>();
  const [cropData, setCropData] = useState('');
  const [size, setSize] = useState<any>();

  const getCropData = () => {
    const cropper = cropperRef.current?.cropper;
    if (typeof cropper !== 'undefined') {
      const imgCrop = cropper.getCroppedCanvas().toDataURL();
      setCropData(imgCrop);
      onSetUrlImg?.(imgCrop);
    }
  };

  const onCropMove = () => {
    const cropper = cropperRef.current?.cropper;
    if (typeof cropper !== 'undefined') {
      const { width, height } = cropper.getCroppedCanvas();
      setSize({ width, height });
    }
  };

  return (
    <MDModal
      {...{
        flat: false,
        open,
        onToggle,
        classModal: styles.ModalCrop,
      }}>
      <>
        <ModalHeader {...{ onClose: onToggle, title: 'Crop Image Details' }} />
        <Cropper
          className={styles.Cropper}
          cropmove={onCropMove}
          ref={cropperRef}
          zoomTo={0.5}
          initialAspectRatio={1}
          preview=".img-preview"
          src={urlImg}
          viewMode={1}
          minCropBoxHeight={10}
          minCropBoxWidth={10}
          background={false}
          responsive
          autoCropArea={1}
          checkOrientation={false}
          guides
          aspectRatio={aspectRatio}
        />

        <div className={styles.CroppedImg}>
          <p>
            {size?.width} x {size?.height}
          </p>

          <MDButton
            type="button"
            onClick={getCropData}
            className={styles.btnCrop}>
            Cắt lớp
          </MDButton>

          {cropData && (
            <figure className={styles.view}>
              <img style={{ width: '100%' }} src={cropData} alt="cropped" />
            </figure>
          )}
        </div>

        <ModalGroupAction
          {...{
            oke: {
              turn: true,
              value: 'Oke',
              action: onToggle,
            },
          }}
        />
      </>
    </MDModal>
  );
};

export default ModalCrop;
