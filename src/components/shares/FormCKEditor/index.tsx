import { MDWrap } from '@components/libraries';
import cx from 'classnames';
import {
  ContentState,
  convertFromHTML,
  convertToRaw,
  EditorState,
} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import dynamic from 'next/dynamic';
import { FC, useState } from 'react';
import styles from './styles.module.scss';

const Editor = dynamic(
  () => import('react-draft-wysiwyg').then((mod) => mod.Editor),
  { ssr: false },
);

export interface IFormCKEditor {
  content: any;
  onChange?: any;
}

const FormCKEditor: FC<IFormCKEditor> = ({
  content,
  onChange = undefined,
  ...args
}) => {
  delete (args as any)['customInput'];

  const [isHtml, setisHtml] = useState(false);

  const editor = !content
    ? EditorState.createEmpty()
    : EditorState.createWithContent(
        ContentState.createFromBlockArray(convertFromHTML(content) as any),
      );

  const [editState, seteditState] = useState(editor);

  const onEditorStateChange = (editorState: EditorState) => {
    seteditState(editorState);
    const content = draftToHtml(convertToRaw(editorState.getCurrentContent()));

    onChange(content);
  };

  const onSwitchHtml = () => {
    setisHtml(!isHtml);
  };

  const onChangeHtml = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const content = event.target.value;
    onChange(content);
  };

  return (
    <div className={styles.FormCKEditor}>
      <MDWrap className={styles.groupActionContent}>
        <button
          type="button"
          onClick={onSwitchHtml}
          className={cx(styles.btn, { [styles.activeEditor]: !isHtml })}>
          Editor
        </button>

        <button
          type="button"
          onClick={onSwitchHtml}
          className={cx(styles.btn, { [styles.activeHtml]: isHtml })}>
          html {'{}'}
        </button>
      </MDWrap>

      {isHtml ? (
        <textarea
          {...args}
          className={styles.sectionHtml}
          placeholder="Nhập nội dung với code html ..."
          rows={15}
          onChange={onChangeHtml}
        />
      ) : (
        <Editor
          editorState={editState}
          wrapperClassName={styles.wrapperClassName}
          editorClassName={styles.editorClassName}
          toolbarClassName={styles.toolbarClassName}
          onEditorStateChange={onEditorStateChange}
        />
      )}
    </div>
  );
};

export default FormCKEditor;
