import BlockFormTitle from './BlockFormTitle';
import BlockPageTitle from './BlockPageTitle';
import FormCKEditor from './FormCKEditor';
import ModalGroupAction from './ModalGroupAction';
import ModalHeader from './ModalHeader';
import TableStandard from './TableStandard';
import UploadImage from './UploadImage';
import UploadImages from './UploadImages';

export {
  UploadImage,
  UploadImages,
  ModalHeader,
  ModalGroupAction,
  TableStandard,
  BlockPageTitle,
  BlockFormTitle,
  FormCKEditor,
};
