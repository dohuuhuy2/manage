export const columns = [
  { name: 'ID', uid: 'id' },
  { name: 'key', uid: 'key' },
  { name: 'title', uid: 'title' },
  { name: 'description', uid: 'description' },
  { name: 'STATE', uid: 'state' },
  { name: 'ACTIONS', uid: 'actions' },
];
