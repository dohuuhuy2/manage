import { MDContainer } from '@components/libraries';
import { BlockPageTitle, TableStandard } from '@components/shares';
import axins from '@process/api';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { urlPage } from '@@init/page';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { columns } from './config';
import styles from './styles.module.scss';

const SEOsPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.getListSEO({}));
  }, [dispatch]);

  const SEO = useAppSelector((state) => state.SEO);

  const onEdit = (item: any) => {
    dispatch(actions.getSEODetail({ key: item.key }));
  };

  const onCreate = () => {
    router.push(urlPage.chiTietSEO, undefined);
    dispatch(actions.selectSEO({}));
    dispatch(actions.saveSEODetail({}));
  };

  const onDetele = async (item: any) => {
    await axins.delete(`SEO/remove/${item._id}`);
    dispatch(actions.getListSEO({}));
    toast.success('Xóa thành công');
  };

  const onRefresh = () => {
    dispatch(actions.getListSEO({}));
  };

  return (
    <MDContainer className={styles.SEOsPage}>
      <BlockPageTitle
        {...{
          title: 'Quản lý SEOs',
          onCreate,
          onRefresh,
        }}
      />

      <TableStandard
        {...{
          columns,
          list: SEO?.list,
          onEdit,
          onDetele,
        }}
      />
    </MDContainer>
  );
};

export default SEOsPage;
