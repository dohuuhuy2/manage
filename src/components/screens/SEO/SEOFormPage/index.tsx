import {
  MDContainer,
  MDFormControl,
  MDFormProvider,
  MDSwitch,
} from '@components/libraries';
import { BlockFormTitle } from '@components/shares';
import axins from '@process/api';
import { valid } from '@process/helper';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { isEqual } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

const SEOsDetailPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { SEO, partner } = useAppSelector((state) => state);
  const { key } = router.query;
  const methods = useForm<any>({
    mode: 'all',
    defaultValues: {
      partnerId: partner.detail?.partnerId,
    },
  });
  const { handleSubmit, setValue } = methods;

  useEffect(() => {
    if (SEO?.detail) {
      Object.keys(SEO?.detail).map((item) =>
        setValue(item, (SEO?.detail as any)[item]),
      );
    }
  }, [setValue, SEO?.detail]);

  const onSubmit = async (data: any) => {
    console.info('🚀 ~ file: index.tsx:49 ~ onSubmit ~ data:', data);
    try {
      if (isEqual(data, SEO.detail)) {
        return toast.warning('Có thay đổi gi đâu mà save dị cưng ?');
      }

      if (key) {
        await axins.patch('SEO/update', JSON.stringify(data));
        toast.success('Cập nhật thành công');
        dispatch(actions.getSEODetail({ key }));
      } else {
        await axins.post('SEO/create', JSON.stringify(data));
        toast.success('Tạo mới thành công');
      }
      dispatch(actions.getListSEO({}));
      data.key && dispatch(actions.getSEODetail({ key: data.key }));
    } catch (err) {
      console.info('err', err);
    }
  };

  const isPublish = isEqual(SEO?.detail?.state, 'publish');

  const onSetState = async () => {
    try {
      await axins.patch(
        'SEO/update',
        JSON.stringify({
          _id: SEO?.detail._id,
          state: isPublish ? 'draft' : 'publish',
        }),
      );
      dispatch(actions.getSEODetail({ key }));
      toast.success('Cập nhật trạng thái thành công');
    } catch (err) {
      console.info(err);
    }
  };

  const onBack = () => {
    router.back();
  };

  const onChangeIndex = (data: any) => {
    setValue('noindex', data, { shouldValidate: true });
  };

  const onChangeFollow = (data: any) => {
    setValue('nofollow', data, { shouldValidate: true });
  };

  return (
    <MDContainer className={styles.SEOsDetailPage}>
      <BlockFormTitle
        {...{
          title: 'Chi tiết SEO page',
          onBack: onBack,
          onSave: handleSubmit(onSubmit),
          onToggleState: onSetState,
          disabledState: !SEO?.detail?.state,
          state: SEO?.detail?.state,
        }}
      />

      <MDFormProvider methods={methods} className={styles.form}>
        <MDFormControl
          {...{
            label: 'noindex',
            customInput: <MDSwitch valueChecked={SEO.detail?.noindex} />,
            onChange: onChangeIndex,
          }}
        />
        <MDFormControl
          {...{
            label: 'nofollow',
            customInput: <MDSwitch valueChecked={SEO.detail?.nofollow} />,
            onChange: onChangeFollow,
          }}
        />
        <MDFormControl
          {...{
            label: 'title',
            rules: valid.required(),
          }}
        />
        <MDFormControl
          {...{
            label: 'key',
          }}
        />
        <MDFormControl
          {...{
            label: 'description',
          }}
        />
        <MDFormControl
          {...{
            label: 'canonical',
          }}
        />
        <div className={styles.wrapOpenGraph}>
          <div className={styles.blockOpenGraph}>
            <p className={styles.subTitle}>openGraph</p>
            <div className={styles.openGraph}>
              <MDFormControl
                {...{
                  label: 'type',
                  name: 'openGraph.type',
                }}
              />
              <MDFormControl
                {...{
                  label: 'url',
                  name: 'openGraph.url',
                }}
              />
              <MDFormControl
                {...{
                  label: 'title',
                  name: 'openGraph.title',
                }}
              />
              <MDFormControl
                {...{
                  label: 'description',
                  name: 'openGraph.description',
                }}
              />
              <MDFormControl
                {...{
                  label: 'site_name',
                  name: 'openGraph.site_name',
                }}
              />

              <div className={styles.blockOpenGraphImages}>
                <p className={styles.subTitle}>images</p>
                <div className={styles.images}>
                  <MDFormControl
                    {...{
                      label: 'url',
                      name: 'openGraph.images[0].url',
                    }}
                  />

                  <MDFormControl
                    {...{
                      label: 'alt',
                      name: 'openGraph.images[0].alt',
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </MDFormProvider>
    </MDContainer>
  );
};

export default SEOsDetailPage;
