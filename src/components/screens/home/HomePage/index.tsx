import info_app from '@process/extra/info_app';
import { listen } from '@process/utils/func';

const HomeScreen = (): JSX.Element => {
  return (
    <div>
      <p>đanng phát triển trang Home...</p>
      <pre>
        <code>{JSON.stringify({ ...listen(), info_app }, undefined, 2)}</code>
      </pre>
    </div>
  );
};

export default HomeScreen;
