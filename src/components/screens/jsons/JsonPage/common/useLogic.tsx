import axins from '@process/api';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import React from 'react';
import { useDispatch } from 'react-redux';

const useLogicJson = () => {
  const dispatch = useDispatch();

  const [modal, setmodal] = React.useState({
    status: false,
    selectItem: {} as any,
    action: 'update',
  });

  const { total, json } = useAppSelector((state) => state);

  const toggleModal = () => {
    setmodal((prev) => ({
      ...prev,
      status: !prev.status,
    }));
  };

  const onCloseModal = () => {
    setmodal((prev) => ({
      ...prev,
      status: false,
    }));
  };

  const selectItem = ({ item }: { item: any }) => {
    setmodal((prev) => ({
      ...prev,
      selectItem: item,
    }));
  };

  const setAction = ({ action }: { action: 'update' | 'create' }) => {
    setmodal((prev) => ({
      ...prev,
      action,
    }));
  };

  const onSelectItem = ({ item }: any) => {
    toggleModal();
    selectItem({ item });
    setAction({ action: 'update' });
  };

  const onAddItem = () => {
    toggleModal();
    selectItem({ item: {} });
    setAction({ action: 'create' });
  };

  const onAction = async (data: any, reset?: any) => {
    let respone;
    if (modal.action === 'create') {
      respone = await axins.post(
        'json/create',
        JSON.stringify({ ...data, partnerId: 'admin' }),
      );
    } else {
      const itemUpdate = {
        ...modal.selectItem,
        ...data,
        partnerId: 'admin',
      };
      respone = await axins.patch('json/update', JSON.stringify(itemUpdate));
    }

    if (respone.data) {
      dispatch(actions.requestJsons({}));
    }

    onCloseModal();
    reset();
  };

  const onDetele = async () => {
    await axins.delete(`json/remove/${modal.selectItem?._id}`);
    dispatch(actions.requestJsons({}));

    onCloseModal();
  };

  return {
    modal,
    total,
    json,
    onDetele,
    onAddItem,
    onSelectItem,
    onAction,
    onCloseModal,
  };
};

export default useLogicJson;
