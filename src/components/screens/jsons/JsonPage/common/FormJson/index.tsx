import { MDFormControl, MDFormProvider, MDWrap } from '@components/libraries';
import { valid } from '@process/helper';
import { isJson } from '@process/utils/func';
import Link from 'next/link';
import JSONInput from 'react-json-editor-ajrm';
import { jsonEN } from '../constants';
import styles from './styles.module.scss';

type FormJsonProps = {
  methods: any;
  isUpdate: boolean;
  urlLink: string;
  item: any;
};

export const FormJson: React.FC<FormJsonProps> = ({
  methods,
  isUpdate,
  urlLink,
  item,
}): JSX.Element => {
  const { setValue } = methods;

  const onChangeJson = (data: any) => {
    setValue('json', data.json, { shouldValidate: true });
  };

  return (
    <>
      <MDFormProvider {...{ methods, className: styles.form, shadown: false }}>
        <MDWrap column className={styles.coverControl}>
          <MDFormControl {...{ label: 'folder', rules: valid.required() }} />
          <MDFormControl {...{ label: 'name', rules: valid.required() }} />

          <MDFormControl
            label="json"
            customInput={
              <JSONInput
                width="100%"
                locale={jsonEN}
                placeholder={isJson(item.json) ? JSON.parse(item.json) : null}
                onKeyPressUpdate
                theme={'light_mitsuketa_tribute'}
                style={{
                  outerBox: {
                    height: 'auto',
                    maxHeight: '550px',
                    width: '100%',
                  },
                  container: {
                    height: 'auto',
                    maxHeight: '550px',
                    width: '100%',
                    overflow: 'scroll',
                  },
                  body: {
                    minHeight: '45px',
                    width: '100%',
                    fontSize: '14px',
                  },
                }}
                onChange={onChangeJson}
              />
            }
          />

          {isUpdate && (
            <div className={styles.urlLink}>
              <Link href={urlLink} target={'_blank'}>
                {urlLink}
              </Link>
            </div>
          )}
        </MDWrap>
      </MDFormProvider>
    </>
  );
};
