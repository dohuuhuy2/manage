import { MDModal } from '@components/libraries';
import { ModalGroupAction, ModalHeader } from '@components/shares';
import { useAppSelector } from '@store/init';
import { isEqual } from 'lodash';
import { MouseEventHandler, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { FormJson } from '../FormJson';
import styles from './styles.module.scss';
import current_env from '@process/envs';

type ModalJsonProps = {
  item: any;
  action: string;
  status: boolean;
  onClose?: any;
  onAction?: (data: any, reset?: any) => Promise<void>;
  onDelete?: MouseEventHandler<HTMLButtonElement>;
};

export const ModalJson: React.FC<ModalJsonProps> = ({
  status,
  item,
  action,
  onClose = undefined,
  onAction = undefined,
  onDelete = undefined,
}): JSX.Element => {
  const methods = useForm<any>({ mode: 'all' });
  const { handleSubmit, setValue, reset } = methods;
  const isUpdate = isEqual(action, 'update');
  const { partner } = useAppSelector((state) => state);

  useEffect(() => {
    if (item) {
      Object.keys(item)?.map((key) => {
        return setValue(key, item[key], { shouldValidate: true });
      });
    }

    setValue('partnerId', partner.select?.partnerId, {
      shouldValidate: true,
    });
  }, [item, partner.select?.partnerId, setValue]);

  const onCloseVsReset = () => {
    onClose();
    reset();
  };

  const urlLink = `${current_env.API_BE}/json/query?partnerId=${item.partnerId}&folder=${item.folder}&name=${item.name}`;

  return (
    <MDModal classModal={styles.modal} open={status} onToggle={onCloseVsReset}>
      <ModalHeader title="Form JSON" onClose={onCloseVsReset} />
      <FormJson {...{ methods, isUpdate, item, urlLink }} />
      <ModalGroupAction
        {...{
          close: {
            turn: true,
            action: onCloseVsReset,
            value: 'Close',
          },
          del: {
            turn: isUpdate,
            action: onDelete,
            value: 'Delete',
          },
          oke: {
            turn: true,
            action: handleSubmit((data) => onAction?.(data, reset)),
            value: 'Save',
          },
        }}
      />
    </MDModal>
  );
};
