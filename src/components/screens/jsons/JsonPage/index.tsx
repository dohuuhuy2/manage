import { MDContainer } from '@components/libraries';
import { BlockPageTitle } from '@components/shares';
import { size } from 'lodash';
import React from 'react';
import { ModalJson } from './common/ModalJson';
import useLogicJson from './common/useLogic';
import styles from './styles.module.scss';

const JsonPage = (): JSX.Element => {
  const {
    modal,
    json,
    onAction,
    onDetele,
    onAddItem,
    onSelectItem,
    onCloseModal,
  } = useLogicJson();

  return (
    <>
      <MDContainer className={styles.JSON}>
        <BlockPageTitle
          {...{
            title: 'Quản lý JSON',
            onCreate: onAddItem,
          }}
        />

        <div className={styles.contents}>
          {size(json.list) > 0 &&
            Object.keys(json.list)?.map((ele: any, key) => {
              const listItem = json.list?.[ele] as any[];
              return (
                <div key={key} className={styles.collapse}>
                  <div className={styles.folder}>
                    <p>{ele}</p>
                  </div>
                  <div className={styles.list}>
                    {listItem.map((item, index) => {
                      return (
                        <div
                          className={styles.card}
                          key={index}
                          onClick={() => {
                            onSelectItem({ item });
                          }}>
                          <div>
                            <div>
                              <p>{item?.name}</p>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}
        </div>
      </MDContainer>

      <ModalJson
        {...({
          item: modal.selectItem,
          action: modal.action,
          status: modal.status,
          onClose: onCloseModal,
          onAction: onAction,
          onDelete: onDetele,
        } as const)}
      />
    </>
  );
};

export default JsonPage;
