import LoginPage from './auth/LoginPage';

import DemoPage from './demo/DemoPage';
import HomePage from './home/HomePage';

import ImagePage from './images/ImagePage';
import JsonPage from './jsons/JsonPage';

import ChoosePartnersPage from './partners/PartnerChoosePage';
import PartnerDetailsPage from './partners/PartnerDetailsPage';
import PartnersPage from './partners/PartnersPage';

import PostDetailsPage from './posts/PostDetailsPage';
import PostPage from './posts/PostPage';

import SEOFormPage from './SEO/SEOFormPage';
import SEOPage from './SEO/SEOPage';

import TagDetailsPage from './tags/TagDetailsPage';
import TagsPage from './tags/TagsPage';

export {
  DemoPage,
  TagsPage,
  TagDetailsPage,
  PostPage,
  PostDetailsPage,
  ChoosePartnersPage,
  PartnersPage,
  PartnerDetailsPage,
  LoginPage,
  HomePage,
  JsonPage,
  ImagePage,
  SEOPage,
  SEOFormPage,
};
