import { urlPage } from '@@init/page';
import { MDContainer } from '@components/libraries';
import { BlockPageTitle, TableStandard } from '@components/shares';
import axins from '@process/api';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { columns } from './config';
import styles from './styles.module.scss';

const PartnersPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.requestPartners({}));
  }, [dispatch]);

  const { partner } = useAppSelector((state) => state);

  const onEdit = (item: any) => {
    dispatch(actions.requestPartner({ id: item.id }));
  };

  const onCreate = () => {
    router.push(
      {
        pathname: urlPage.chiTietDoiTac,
        query: {
          action: 'create',
        },
      },
      undefined,
    );
    dispatch(actions.selectPartner({}));
    dispatch(actions.savePartner({ data: {} }));
  };

  const onDetele = async (item: any) => {
    await axins.delete(`partners/remove/${item.id}`);
    dispatch(actions.requestPartners({}));
    toast.success('Xóa thành công');
  };

  const onRefresh = () => {
    dispatch(actions.requestPartners({}));
  };

  return (
    <MDContainer className={styles.PartnersPage}>
      <BlockPageTitle
        {...{
          title: 'Quản Lý Website (Partners)',
          onCreate,
          onRefresh,
        }}
      />

      <TableStandard
        {...{
          columns,
          list: partner?.list,
          onEdit,
          onDetele,
        }}
      />
    </MDContainer>
  );
};

export default PartnersPage;
