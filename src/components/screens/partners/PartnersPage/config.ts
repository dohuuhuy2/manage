export const columns = [
  { name: 'ID', uid: 'id' },
  { name: 'NAME', uid: 'name' },
  { name: 'PARTNERID', uid: 'partnerId' },
  { name: 'DOMAIN', uid: 'domain' },
  { name: 'STATE', uid: 'state' },
  { name: 'ACTIONS', uid: 'actions' },
];
