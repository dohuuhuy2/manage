import {
  MDContainer,
  MDFormControl,
  MDFormProvider,
} from '@components/libraries';
import { BlockFormTitle, UploadImage } from '@components/shares';
import axins from '@process/api';
import { valid } from '@process/helper';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { isEqual } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

export type PartnerFormType = {
  action?: 'update' | 'create';
};

const PartnerFormPage: React.FC<PartnerFormType> = ({}): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { partner } = useAppSelector((state) => state);
  const { action } = router.query;
  const isCreate = isEqual(action, 'create');

  const methods = useForm<any>({ mode: 'all' });
  const { handleSubmit, setValue } = methods;
  const isPublish = isEqual(partner?.detail?.state, 'publish');

  useEffect(() => {
    dispatch(actions.requestPosts({}));
  }, [dispatch]);

  useEffect(() => {
    if (partner?.detail) {
      Object.keys(partner?.detail).map((item) =>
        setValue(item, (partner?.detail as any)[item]),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [partner?.detail]);

  const onSubmit = async (data: any) => {
    console.info('🚀 ~ file: index.tsx:49 ~ onSubmit ~ data:', data);
    try {
      if (isEqual(data, partner.detail)) {
        // return toast.warning('Có thay đổi gi đâu mà save dị cưng ?');
      }

      let rs;
      if (isCreate) {
        rs = await axins.post('partners/create', JSON.stringify(data));
        toast.success('Tạo mới thành công');
      } else {
        rs = await axins.patch('partners/update', JSON.stringify(data));
        toast.success('Cập nhật thành công');
      }

      data.partnerId && dispatch(actions.savePartner({ data: rs.data }));
      dispatch(actions.requestPartners({}));
    } catch (err) {
      console.info('err', err);
    }
  };

  const onSetState = async () => {
    try {
      const { data } = await axins.patch(
        'partners/update',
        JSON.stringify({
          id: partner?.detail.id,
          state: isPublish ? 'draft' : 'publish',
        }),
      );
      dispatch(actions.savePartner({ data }));
      toast.success('Cập nhật trạng thái thành công');
    } catch (err) {
      console.info(err);
    }
  };

  const onBack = () => {
    router.back();
  };

  const preCondition = {
    partnerId: partner.detail.partnerId,
    folder: 'partners',
  };

  const onChangeMedia = (data: any) => {
    Object.keys(data).forEach((key) => {
      setValue(key, data[key]?.image_url || null, { shouldValidate: true });
    });
  };

  return (
    <MDContainer className={styles.PartnerFormPage}>
      <BlockFormTitle
        {...{
          title: 'Thông tin chi tiết',
          onBack: onBack,
          onSave: handleSubmit(onSubmit),
          onToggleState: onSetState,
          disabledState: !partner?.detail?.state,
          state: partner?.detail?.state,
        }}
      />

      <MDFormProvider methods={methods} className={styles.form}>
        <div className={styles.formLeft}>
          <div className={styles.wrap}>
            <MDFormControl
              {...{
                label: 'name',
                rules: Object.assign(valid.required()),
              }}
            />

            <MDFormControl
              {...{
                label: 'partnerId',
                rules: Object.assign(valid.required()),
                readOnly: true,
              }}
            />

            <MDFormControl
              {...{
                label: 'fullname',
                rules: Object.assign(valid.required()),
              }}
            />

            <MDFormControl {...{ label: 'birthdate' }} />

            <MDFormControl
              {...{
                label: 'phone',
                rules: Object.assign(valid.required()),
              }}
            />

            <MDFormControl {...{ label: 'latitude' }} />
            <MDFormControl {...{ label: 'longitude' }} />
            <MDFormControl
              {...{
                label: 'maps',
              }}
            />

            <MDFormControl
              {...{
                label: 'address',
                textArea: true,
                fullWidth: true,
                row: 2,
              }}
            />
          </div>

          <div className={styles.wrap}>
            <MDFormControl
              {...{
                label: 'facebook',
              }}
            />
            <MDFormControl {...{ label: 'zalo' }} />
            <MDFormControl {...{ label: 'tiktok' }} />
            <MDFormControl {...{ label: 'youtube' }} />
          </div>
        </div>

        <div className={styles.formRight}>
          <div className={styles.wrap}>
            {' '}
            <MDFormControl
              {...{
                label: 'logo',
                onChange: onChangeMedia,
                customInput: (
                  <UploadImage
                    {...{
                      preCondition,
                    }}
                  />
                ),
              }}
            />
            <MDFormControl
              {...{
                label: 'favicon',
                onChange: onChangeMedia,
                customInput: (
                  <UploadImage
                    {...{
                      preCondition,
                    }}
                  />
                ),
              }}
            />
            <MDFormControl
              {...{
                label: 'domain',
                rules: Object.assign(valid.required()),
              }}
            />
            <MDFormControl
              {...{
                label: 'title',
                rules: Object.assign(valid.required()),
              }}
            />
            <MDFormControl
              {...{ label: 'description', textArea: true, row: 2 }}
            />
          </div>
        </div>
      </MDFormProvider>
    </MDContainer>
  );
};

export default PartnerFormPage;
