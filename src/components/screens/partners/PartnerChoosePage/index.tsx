import { MDContainer, MDWrap } from '@components/libraries';
import LCLoading from '@components/libraries/LCLoading';
import { randomColor } from '@process/utils/css';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { urlPage } from '@@init/page';
import { size } from 'lodash';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import styles from './styles.module.scss';

const ChoosePartnersPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { partner } = useAppSelector((state) => state);

  if (size(partner?.list) < 1) {
    return <LCLoading />;
  }

  const onSelectPartner = (item: any) => {
    dispatch(actions.selectPartner({ item }));

    setTimeout(() => {
      router.push(urlPage.home, undefined);
    }, 500);
  };

  return (
    <MDContainer className={styles.ChoosePartnersPage}>
      <MDWrap column>
        <MDWrap>
          <h2>Chọn Đối Tác</h2>
        </MDWrap>
        <MDWrap className={styles.listPartner} wrap>
          {partner?.list?.map((item: any) => {
            return (
              <div
                key={item?.name}
                className={styles.item}
                onClick={() => onSelectPartner(item)}
                style={{
                  backgroundColor: randomColor(),
                }}>
                <p>{item?.name}</p>
              </div>
            );
          })}
        </MDWrap>
      </MDWrap>
    </MDContainer>
  );
};

export default ChoosePartnersPage;
