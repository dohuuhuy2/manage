import {
  MDButton,
  MDContainer,
  MDFormControl,
  MDFormProvider,
  MDWrap,
} from '@components/libraries';
import UploadImage from '@components/shares/UploadImage';
import { useAppSelector } from '@store/init';
import { useForm } from 'react-hook-form';
import styles from './styles.module.scss';

const DemoPage = (): JSX.Element => {
  const methods = useForm<any>({ mode: 'all' });

  const partner = useAppSelector((state) => state.partner);

  const { handleSubmit, setValue } = methods;

  const submit = (data: any) => {
    console.info('data', data);
  };

  const onChangeImage = (data: any) => {
    Object.keys(data).forEach((key) => {
      setValue(key, data[key]?.image_url || null, { shouldValidate: true });
    });
  };

  return (
    <MDContainer className={styles.containDemo}>
      <MDWrap column>
        <MDFormProvider methods={methods} className={styles.form}>
          <MDWrap column>
            <MDFormControl
              label="test upload b"
              name="b"
              onChange={onChangeImage}
              customInput={
                <UploadImage
                  preCondition={{
                    partnerId: partner.detail.partnerId,
                    folder: 'tags',
                  }}
                />
              }
            />

            <MDFormControl
              label="test upload a"
              name="a"
              onChange={onChangeImage}
              customInput={
                <UploadImage
                  preCondition={{
                    partnerId: partner.detail.partnerId,
                    folder: 'tags',
                  }}
                />
              }
            />
            <MDButton value={'submit'} onClick={handleSubmit(submit)} />
          </MDWrap>
        </MDFormProvider>
      </MDWrap>
    </MDContainer>
  );
};

export default DemoPage;
