import LoginIcon from '@assets/icons/LoginIcon';
import {
  BoxIcon,
  MDButton,
  MDContainer,
  MDFormControl,
  MDFormProvider,
  MDWrap,
} from '@components/libraries';
import { optionErrorToast } from '@process/configs/toast-config';
import { valid } from '@process/helper';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { urlPage } from '@@init/page';
import { find, isEqual } from 'lodash';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

const LoginPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { partner } = useAppSelector((state) => state);
  const methods = useForm<any>({ mode: 'all' });
  const { handleSubmit } = methods;

  const onSubmit = async (data: any) => {
    const findUser = find(users, { username: data.username });

    if (findUser) {
      if (findUser?.password === data.password) {
        if (isEqual(findUser.role, 'client')) {
          const itemPartner = find(partner.list, {
            partnerId: findUser.partnerId,
          });
          dispatch(actions.savePartner({ data: itemPartner }));
          router.push(urlPage.home, undefined);
        } else {
          router.push(urlPage.chonDoiTac, undefined);
        }
        dispatch(actions.setInfo({ data: findUser }));
      } else {
        toast.error('Mật khẩu không đúng', optionErrorToast);
      }
    } else {
      toast.error('Tài khoản không đúng', optionErrorToast);
    }
  };

  return (
    <MDContainer className={styles.LoginPage}>
      <MDFormProvider methods={methods} className={styles.form}>
        <MDWrap column>
          <MDWrap className={styles.formBody} column>
            <MDFormControl
              {...{
                label: 'Tài khoản',
                name: 'username',
                rules: valid.required(),
              }}
            />
            <MDFormControl
              {...{
                label: 'Mật khẩu',
                name: 'password',
                rules: valid.required(),
              }}
            />
          </MDWrap>
          <MDWrap className={styles.formFooter} center>
            <MDButton
              value="Đăng nhập"
              onClick={handleSubmit(onSubmit)}
              iconLeft={
                <BoxIcon setting={{ size: 24 }}>
                  <LoginIcon />
                </BoxIcon>
              }
            />
          </MDWrap>
        </MDWrap>
      </MDFormProvider>
    </MDContainer>
  );
};

export default LoginPage;

const users = [
  {
    username: 'huyi',
    password: '1234',
    role: 'client',
    partnerId: 'huyi',
  },
  {
    username: 'admin',
    password: '1234',
    role: 'admin',
    partnerId: 'admin',
  },
];
