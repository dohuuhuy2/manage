export const columns = [
  { name: 'ID', uid: 'id' },
  { name: 'NAME', uid: 'name' },
  { name: 'TITLE', uid: 'title' },
  { name: 'description', uid: 'description' },
  { name: 'SLUG', uid: 'slug' },
  { name: 'STATE', uid: 'state' },
  { name: 'ACTIONS', uid: 'actions' },
];
