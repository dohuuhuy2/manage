import { urlPage } from '@@init/page';
import { MDContainer } from '@components/libraries';
import { BlockPageTitle, TableStandard } from '@components/shares';
import axins from '@process/api';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { columns } from './config';
import styles from './styles.module.scss';

const TagsPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.requestTags({}));
  }, [dispatch]);

  const { tag } = useAppSelector((state) => state);

  const onEdit = (item: any) => {
    dispatch(actions.selectTag({ item }));
    dispatch(actions.requestTag({ slug: item.slug }));
  };

  const onRefresher = () => {
    dispatch(actions.requestTags({}));
  };

  const onCreate = () => {
    router.push(
      {
        pathname: urlPage.chiTietDanhMuc,
        query: {
          action: 'create',
        },
      },
      undefined,
    );
    dispatch(actions.selectTag({}));
    dispatch(actions.saveTag({}));
  };

  const onDetele = async (item: any) => {
    await axins.delete(`tags/remove/${item._id}`);
    dispatch(actions.requestTags({}));
    toast.success('Xóa thành công');
  };

  return (
    <MDContainer className={styles.TagsPage}>
      <BlockPageTitle
        {...{
          title: 'Quản lý danh mục (Tag)',
          onCreate,
          onRefresh: onRefresher,
        }}
      />

      <TableStandard
        {...{
          columns,
          list: tag?.list,
          onEdit,
          onDetele,
        }}
      />
    </MDContainer>
  );
};

export default TagsPage;
