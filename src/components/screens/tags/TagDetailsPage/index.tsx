import { urlPage } from '@@init/page';
import {
  MDContainer,
  MDFormControl,
  MDFormProvider,
} from '@components/libraries';
import { BlockFormTitle, UploadImage } from '@components/shares';
import axins from '@process/api';
import { quick, valid } from '@process/helper';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { isEqual } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

const TagDetailsPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { tag, partner } = useAppSelector((state) => state);
  const { action } = router.query;
  const isCreate = isEqual(action, 'create');
  const isPublish = isEqual(tag.detail?.state, 'publish');

  const methods = useForm<any>({
    mode: 'all',
    defaultValues: {
      partnerId: partner.detail?.partnerId,
    },
  });
  const { handleSubmit, setValue, reset } = methods;

  useEffect(() => {
    if (tag.detail.slug) {
      Object.keys(tag.detail).map((item) =>
        setValue(item, (tag.detail as any)[item]),
      );
    }
    if (isCreate) {
      reset();
    }
  }, [dispatch, setValue, tag.detail, isCreate, reset]);

  const onSubmit = async (data: any) => {
    try {
      if (isEqual(data, tag.detail)) {
        return toast.warning('Có thay đổi gi đâu mà save dị cưng ?');
      }
      let rs;

      if (isCreate) {
        rs = await axins.post('tags/create', JSON.stringify(data));
        toast.success('Tạo mới thành công');
      } else {
        rs = await axins.patch('tags/update', JSON.stringify(data));
        toast.success('Cập nhật thành công');
      }

      rs.data && dispatch(actions.saveTag({ data: rs.data }));
    } catch (err) {
      console.info('err', err);
    }
  };

  const onChangeName = (event: any) => {
    const valueName = event.target.value;
    const valueSlug = quick.slugify(valueName);
    setValue('slug', valueSlug, { shouldValidate: true });
  };

  const onSetState = async () => {
    try {
      await axins.patch(
        'tags/update',
        JSON.stringify({
          _id: tag.detail._id,
          state: isPublish ? 'draft' : 'publish',
        }),
      );
      if (tag?.detail?.slug) {
        dispatch(actions.requestTag({ slug: tag?.detail?.slug }));
      }
      toast.success('Cập nhật trạng thái thành công');
    } catch (err) {
      console.info(err);
    }
  };

  const onBack = () => {
    router.push(urlPage.quanlyDanhMuc);
  };

  const preCondition = {
    partnerId: partner.detail.partnerId,
    folder: 'tags',
    platform: 'web',
  };

  const onChangeImage = (data: any) => {
    setValue('image', data?.image?.image_url);
  };

  const onChangeBackground = (data: any) => {
    setValue('background', data?.image?.image_url);
  };

  const onCreate = () => {
    router.push({
      pathname: urlPage.chiTietDanhMuc,
      query: {
        action: 'create',
      },
    });

    dispatch(actions.selectTag({ item: {} }));
    dispatch(actions.saveTag({ data: {} }));
  };

  return (
    <MDContainer className={styles.TagDetailsPage}>
      <BlockFormTitle
        {...{
          title: 'Chi tiết danh mục',
          onBack,
          onSave: handleSubmit(onSubmit),
          onCreate,
          onToggleState: onSetState,
          disabledState: !tag?.detail?.state,
          state: tag?.detail?.state,
        }}
      />

      <MDFormProvider {...{ methods }} className={styles.form}>
        <MDFormControl
          {...{
            label: 'Short name',
            name: 'name',
            onChange: onChangeName,
          }}
        />
        <MDFormControl
          {...{
            label: 'slug',
            rules: Object.assign(valid.required()),
          }}
        />

        <MDFormControl
          {...{
            label: 'title',
            rules: Object.assign(valid.required()),
          }}
        />

        <MDFormControl
          {...{
            label: 'image',
            name: 'image',
            onChange: onChangeImage,
            customInput: (
              <UploadImage
                {...{
                  preCondition,
                }}
              />
            ),
          }}
        />

        <MDFormControl textArea {...{ label: 'description' }} />

        <MDFormControl
          {...{
            label: 'background',
            onChange: onChangeBackground,
            customInput: (
              <UploadImage
                {...{
                  preCondition,
                }}
              />
            ),
          }}
        />
      </MDFormProvider>
    </MDContainer>
  );
};

export default TagDetailsPage;
