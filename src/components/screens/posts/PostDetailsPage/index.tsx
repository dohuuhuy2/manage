import {
  MDContainer,
  MDDropdownList,
  MDFormControl,
  MDFormProvider,
  MDSwitch,
} from '@components/libraries';
import { BlockFormTitle, FormCKEditor, UploadImage } from '@components/shares';
import axins from '@process/api';
import { quick, valid } from '@process/helper';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { isEqual } from 'lodash';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

const PostDetailsPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { tag, post, partner } = useAppSelector((state) => state);

  const { action } = router.query;
  const isCreate = isEqual(action, 'create');
  const isPublish = isEqual(post.detail?.state, 'publish');
  const methods = useForm<any>({
    mode: 'all',
    defaultValues: {
      partnerId: partner.detail?.partnerId,
      pin: false,
      authors: 'Ban Biên Tập',
    },
  });
  const { handleSubmit, setValue } = methods;

  useEffect(() => {
    dispatch(actions.requestTags({}));
  }, [dispatch]);

  useEffect(() => {
    if (post.detail) {
      Object.keys(post.detail).forEach((item) => {
        setValue(item, (post.detail as any)[item]);
      });
    } else {
      setValue('state', 'draft');
    }
  }, [post.detail, setValue]);

  const onSubmit = async (data: any) => {
    try {
      if (isEqual(data, post.detail)) {
        return toast.warning('Có thay đổi gi đâu mà save dị cưng ?');
      }

      let rs;
      if (isCreate) {
        rs = await axins.post('posts/create', JSON.stringify(data));
        toast.success('Tạo mới thành công');
      } else {
        rs = await axins.patch('posts/update', JSON.stringify(data));
        toast.success('Cập nhật thành công');
      }
      rs.data && dispatch(actions.savePostDetail({ data: rs.data }));
    } catch (err) {
      console.info('err', err);
    }
  };

  const onSetState = async () => {
    try {
      const rs = await axins.patch(
        'posts/update',
        JSON.stringify({
          _id: post.detail._id,
          state: isPublish ? 'draft' : 'publish',
        }),
      );
      dispatch(actions.savePostDetail({ data: rs.data }));
      toast.success('Cập nhật trạng thái thành công');
    } catch (err) {
      console.info(err);
    }
  };

  const onBack = () => {
    router.back();
  };

  const onChangeName = (event: any) => {
    const valueName = event.target.value;
    const valueSlug = quick.slugify(valueName);
    setValue('slug', valueSlug, { shouldValidate: true });
  };

  const onChangeTags = (data: any) => {
    setValue('tags', data, { shouldValidate: true });
  };

  const onChangeSwitch = (data: any) => {
    setValue('pin', data, { shouldValidate: true });
  };

  const onChangeContent = (data: any) => {
    setValue('content', data, { shouldValidate: true });
  };

  const preCondition = {
    partnerId: partner.detail.partnerId,
    folder: 'post',
    platform: 'web',
  };

  const onChangeImage = (data: any) => {
    setValue('image', data.image.image_url, { shouldValidate: true });
  };

  return (
    <MDContainer className={styles.PostDetailsPage}>
      <BlockFormTitle
        {...{
          title: 'Chi tiết bài viết',
          onBack: onBack,
          onSave: handleSubmit(onSubmit),
          onToggleState: onSetState,
          disabledState: !post?.detail?.state,
          state: post?.detail?.state,
        }}
      />

      <MDFormProvider methods={methods} className={styles.form}>
        <MDFormControl
          {...{
            direction: 'hor',
            label: 'Tuyển chọn',
            name: 'pin',
            customInput: <MDSwitch valueChecked={post?.detail?.pin} />,
            onChange: onChangeSwitch,
            fullWidth: true,
          }}
        />

        <MDFormControl
          {...{
            label: 'title',
            onChange: onChangeName,
            rules: Object.assign(valid.required()),
          }}
        />

        <MDFormControl
          {...{
            label: 'slug',
            rules: Object.assign(valid.required()),
          }}
        />

        <MDFormControl label="description" textArea fullWidth rows={5} />

        <MDFormControl
          {...{
            label: 'content',
            fullWidth: true,
            rules: Object.assign(valid.required()),
            customInput: (
              <FormCKEditor
                content={post?.detail?.content}
                onChange={onChangeContent}
              />
            ),
          }}
        />

        <MDFormControl
          {...{
            label: 'image',
            onChange: onChangeImage,
            customInput: (
              <UploadImage
                {...{
                  preCondition,
                }}
              />
            ),
          }}
        />
        <MDFormControl
          {...{
            label: 'authors',
            rules: Object.assign(valid.required()),
          }}
        />

        <MDFormControl
          {...{ label: 'tags' }}
          customInput={
            <MDDropdownList
              multiple
              multipleSeleted={post?.detail?.tags}
              multipleKey="slug"
              switchSearch
              list={tag.list}
              title="Chọn danh mục"
              keyShow="name"
              onChange={onChangeTags}
              switchMain
            />
          }
        />

        <MDFormControl
          {...{
            label: 'keywords',
          }}
        />
      </MDFormProvider>
    </MDContainer>
  );
};

export default PostDetailsPage;
