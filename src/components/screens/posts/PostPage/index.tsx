import { MDContainer } from '@components/libraries';
import { BlockPageTitle, TableStandard } from '@components/shares';
import axins from '@process/api';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { urlPage } from '@@init/page';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { columns } from './config';
import styles from './styles.module.scss';

const PostPage = (): JSX.Element => {
  const router = useRouter();
  const dispatch = useDispatch();

  const { post } = useAppSelector((state) => state);

  useEffect(() => {
    dispatch(actions.requestPosts({}));
  }, [dispatch]);

  const handle = {
    onEdit: (item: any) => {
      dispatch(actions.selectPost({ item }));
      dispatch(actions.getPostDetail({ slug: item.slug }));
    },
    onCreate: () => {
      router.push(
        { pathname: urlPage.chiTietBaiViet, query: { action: 'create' } },
        undefined,
      );
      dispatch(actions.selectPost({}));
      dispatch(actions.savePostDetail({}));
    },
    onDetele: async (item: any) => {
      await axins.delete(`posts/remove/${item._id}`);
      dispatch(actions.requestPosts({}));
      toast.success('Xóa thành công');
    },
    onRefresh: () => {
      dispatch(actions.requestPosts({}));
    },
  };

  return (
    <MDContainer className={styles.PostPage}>
      <BlockPageTitle
        {...{
          title: 'Quản lý bài viết (Posts)',
          onCreate: handle.onCreate,
          onRefresh: handle.onRefresh,
        }}
      />

      <TableStandard
        {...{
          columns,
          list: post?.list.list,
          onEdit: handle.onEdit,
          onDetele: handle.onDetele,
        }}
      />
    </MDContainer>
  );
};

export default PostPage;
