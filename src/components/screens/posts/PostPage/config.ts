export const columns = [
  { name: 'id', uid: '_id' },
  { name: 'title', uid: 'title' },
  { name: 'slug', uid: 'slug' },
  { name: 'state', uid: 'state' },
  { name: 'actions', uid: 'actions' },
];
