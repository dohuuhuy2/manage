import FolderIcon from '@assets/icons/FolderIcon';
import { BoxIcon, MDContainer } from '@components/libraries';
import { BlockPageTitle } from '@components/shares';
import { size } from 'lodash';
import React from 'react';
import { ModalImages } from './common/ModalImages';
import useLogic from './common/useLogic';
import styles from './styles.module.scss';

const ImagePage = (): JSX.Element => {
  const {
    image,
    selectItem,
    action,
    stateModal,
    onAction,
    onAddItem,
    onCloseModal,
    onDelete,
    onSelectItem,
    onRefresh,
  } = useLogic();

  return (
    <React.Fragment>
      <MDContainer className={styles.images}>
        <BlockPageTitle
          {...{
            title: 'Quản lý Images',
            onCreate: onAddItem,
            onRefresh: onRefresh,
          }}
        />

        <section className={styles.content}>
          <div className={styles.collapse}>
            {size(image?.list) > 0 &&
              Object.keys(image.list)?.map((ele: any, index) => {
                const listItem = image.list?.[ele] as any[];
                return (
                  <div key={index}>
                    <div className={styles.folder}>
                      <BoxIcon setting={{ size: 28 }}>
                        <FolderIcon />
                      </BoxIcon>
                      <p>{ele}</p>
                    </div>
                    <div className={styles.list}>
                      {listItem?.map((item, key) => {
                        return (
                          <div
                            className={styles.card}
                            key={key}
                            onClick={() => onSelectItem(item)}>
                            <figure className={styles.cardView}>
                              <img
                                src={`${item?.image_url}?t=${Date.now()}`}
                                alt={item?.name}
                              />
                            </figure>
                            <div className={styles.cardBody}>
                              <p className={styles.name}>{item?.name}</p>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
          </div>
        </section>
      </MDContainer>

      <ModalImages
        {...({
          item: selectItem,
          action: action,
          status: stateModal,
          onClose: onCloseModal,
          onAction: onAction,
          onDelete: onDelete,
        } as const)}
      />
    </React.Fragment>
  );
};

export default React.memo(ImagePage);
