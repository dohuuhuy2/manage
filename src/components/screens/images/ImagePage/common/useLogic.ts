/* eslint-disable no-constant-condition */
import axins from '@process/api';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import { AxiosResponse } from 'axios';
import { isEqual } from 'lodash';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';

const useLogic = () => {
  const dispatch = useDispatch();

  const { image } = useAppSelector((state) => state);
  const [selectItem, setSelectItem] = useState<any>({});
  const [action, setAction] = useState('update');
  const [stateModal, setStateModal] = React.useState(false);

  const onRefresh = () => {
    dispatch(actions.requestImages());
  };

  const onToggleModal = () => {
    setStateModal(!stateModal);
  };

  const onCloseModal = () => {
    setStateModal(false);
    setSelectItem({});
  };

  const onAddItem = () => {
    onToggleModal();
    setSelectItem({});
    setAction('new');
  };

  const onSelectItem = (item = {}) => {
    onToggleModal();
    setSelectItem(item);
    setAction('update');
  };

  const onAction = async (data: any, reset?: any) => {
    if (data?.partnerId) {
      const isCreate = isEqual(action, 'new');
      const isUpdate = isEqual(action, 'update');
      const img = isUpdate ? data.editImg : data.images;

      const formData = new FormData();
      formData.append('partnerId', data?.partnerId);
      formData.append('folder', data?.folder);
      formData.append('name', data?.name);
      formData.append('platform', data?.platform);

      img && formData.append('file', img[0], img[0]?.name);

      let reponse: AxiosResponse;
      try {
        if (isCreate) {
          reponse = await axins.post('/images/upload', formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });
        } else {
          reponse = await axins.patch(
            `images/update/${selectItem?._id}`,
            formData,
            {
              headers: {
                'Content-Type': 'multipart/form-data',
              },
            },
          );
        }

        if (reponse?.data) {
          toast.success('Lưu thành công !');
          dispatch(actions.requestImages());
        } else {
          toast.error('Đã có lỗi !');
        }
      } catch (err) {
        console.info('err onAction Image', err);
        toast.error('Đã có lỗi !');
      }
    } else {
      toast.error('Thiếu partnerId !');
    }

    reset();
    onCloseModal();
  };

  const onDelete = async () => {
    await axins.delete(`images/remove/${selectItem?._id}`);
    dispatch(actions.requestImages());
    onCloseModal();
  };

  return {
    image,
    selectItem,
    action,
    stateModal,
    onAction,
    onAddItem,
    onCloseModal,
    onDelete,
    onToggleModal,
    onSelectItem,
    onRefresh,
  };
};

export default useLogic;
