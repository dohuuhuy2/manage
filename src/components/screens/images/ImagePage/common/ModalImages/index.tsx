/* eslint-disable react-hooks/exhaustive-deps */
import { MDModal } from '@components/libraries';
import { ModalGroupAction, ModalHeader } from '@components/shares';
import { useAppSelector } from '@store/init';
import { isEqual } from 'lodash';
import React, { MouseEventHandler, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { FormImage } from '../FormImage';
import styles from './styles.module.scss';

type ModalImagesProps = {
  item: any;
  action: string;
  status: boolean;
  onClose?: any;
  onAction?: (data: any, reset?: any) => Promise<void>;
  onDelete?: MouseEventHandler<HTMLButtonElement>;
};

export const ModalImages: React.FC<ModalImagesProps> = ({
  status,
  onClose,
  item,
  onAction,
  action,
  onDelete,
}): JSX.Element => {
  const methods = useForm<any>({ mode: 'all' });
  const { handleSubmit, setValue, reset } = methods;
  const { partner } = useAppSelector((state) => state);
  const isUpdate = isEqual(action, 'update');

  useEffect(() => {
    action &&
      setValue('partnerId', partner.detail?.partnerId, {
        shouldValidate: true,
      });
  }, [action, partner.detail?.partnerId]);

  useEffect(() => {
    if (item) {
      Object.keys(item)?.map((key) => {
        return setValue(key, item[key], { shouldValidate: true });
      });
    }
  }, [item]);

  const onCloseVsReset = () => {
    onClose();
    reset();
  };

  const urlImg = item?.image_url;

  return (
    <MDModal classModal={styles.modal} open={status} onToggle={onCloseVsReset}>
      <>
        <ModalHeader title="Form Image" onClose={onCloseVsReset} />

        <FormImage {...{ methods, urlLink: urlImg, item, isUpdate }} />

        <ModalGroupAction
          {...{
            close: {
              turn: true,
              action: onCloseVsReset,
              value: 'Close',
            },
            del: {
              turn: isUpdate,
              action: onDelete,
              value: 'Delete',
            },
            oke: {
              turn: true,
              action: handleSubmit((data) => onAction?.(data, reset)),
              value: 'Save',
            },
          }}
        />
      </>
    </MDModal>
  );
};
