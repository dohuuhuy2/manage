import { MDFormControl, MDFormProvider, MDWrap } from '@components/libraries';
import { valid } from '@process/helper';
import Link from 'next/link';
import { useState } from 'react';
import styles from './styles.module.scss';

type FormImageProps = {
  methods: any;
  isUpdate: boolean;
  urlLink: string;
  item: any;
};

export const FormImage: React.FC<FormImageProps> = ({
  methods,
  isUpdate,
  urlLink,
  item,
}): JSX.Element => {
  const [urlImg, setUrlImg] = useState(item?.image_url || '');

  const onChangeImage = (event: any) => {
    const { files } = event.target;
    const url = URL?.createObjectURL(files[0]);
    setUrlImg(url);
  };

  return (
    <>
      <MDFormProvider {...{ methods, className: styles.form, shadown: false }}>
        <MDWrap wrap className={styles.coverControl}>
          <MDFormControl {...{ label: 'folder', rules: valid.required() }} />
          <MDFormControl {...{ label: 'name', rules: valid.required() }} />
          <MDFormControl {...{ label: 'platform', rules: valid.required() }} />

          {urlImg && (
            <figure className={styles.viewImage}>
              <img
                width={320}
                height={180}
                src={urlImg}
                alt=""
                style={{ objectFit: 'contain' }}
              />
            </figure>
          )}

          <MDFormControl
            {...{
              label: isUpdate ? 'update image ?' : 'image',
              type: 'file',
              name: isUpdate ? 'editImg' : 'images',
              rules: isUpdate ? {} : valid.required(),
              onChange: onChangeImage,
            }}
          />

          {isUpdate && (
            <div className={styles.boxUrlImg}>
              <Link href={`${urlLink}?t=${Date.now()}`} target={'_blank'}>
                <p>{`${urlLink}?t=${Date.now()}`}</p>
              </Link>
            </div>
          )}
        </MDWrap>
      </MDFormProvider>
    </>
  );
};
