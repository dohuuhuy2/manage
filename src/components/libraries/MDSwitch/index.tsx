import cx from 'classnames';
import { DetailedHTMLProps, InputHTMLAttributes, useState } from 'react';
import styles from './styles.module.scss';

export type IMDSwitch = {
  size?: 'small' | 'medium' | 'large';
  valueChecked?: boolean;
  onChange?: any;
  type?: string;
  single?: boolean;
} & DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

const MDSwitch: React.FC<IMDSwitch> = ({ ...props }: any): JSX.Element => {
  const { size = 'large', valueChecked, ...args } = props;

  const [isChecked, setIsChecked] = useState(valueChecked || false);

  const onToggle = () => {
    setIsChecked(!isChecked);
    args?.onChange?.(!isChecked);
  };

  return (
    <label className={cx(styles.MDSwitch, styles[size])}>
      <input
        {...{
          name: args?.name || 'box',
          type: 'checkbox',
        }}
        onChange={onToggle}
        checked={isChecked}
      />
      <div className={cx(styles.slider, styles.round)} />
    </label>
  );
};

export default MDSwitch;
