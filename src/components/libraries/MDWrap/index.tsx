import { Tag } from '@process/define';
import classNames from 'classnames';
import styles from './styles.module.scss';

export type IWrap = {
  id?: string;
  tag?: Tag;
  className?: string;
  children?: React.ReactNode | string;
  section?: boolean;
  paragraph?: boolean;
  invade?: boolean;
  column?: boolean;
  center?: boolean;
  between?: boolean;
  wrap?: boolean;
  right?: boolean;
  alignItemCenter?: boolean;
} & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

const MDWrap: React.FC<IWrap> = ({
  tag,
  className,
  id,
  children,
  section,
  paragraph,
  invade,
  column,
  center,
  between,
  wrap,
  right,
  alignItemCenter,
  ...args
}) => {
  const Tag = switchTag(tag, section, paragraph);

  return (
    <Tag
      {...{
        id,
        className: classNames(styles.MDWrap, className, {
          [styles.invade]: invade,
          [styles.column]: column,
          [styles.center]: center,
          [styles.between]: between,
          [styles.wrap]: wrap,
          [styles.right]: right,
          [styles.alignItemCenter]: alignItemCenter,
        }),
        ...args,
      }}>
      {children}
    </Tag>
  );
};

export default MDWrap;

const switchTag = (tag: any, section?: boolean, paragraph?: boolean) => {
  let _tag;

  if (tag) {
    _tag = tag;
  } else {
    if (section) _tag = 'section';
    else if (paragraph) _tag = 'paragraph';
    else {
      _tag = 'div';
    }
  }

  return _tag;
};
