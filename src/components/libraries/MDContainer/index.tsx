import cx from 'classnames';
import { ReactNode } from 'react';
import styles from './styles.module.scss';
type Props = {
  children?: ReactNode;
  fluid?: boolean;
  fixed?: boolean;
  className?: any;
  style?: any;
  xxl?: boolean;
  xl?: boolean;
  md?: boolean;
  sm?: boolean;
  id?: string;
  tag?: keyof JSX.IntrinsicElements | string;
};
const MDContainer = ({
  children,
  fluid,
  fixed,
  className: classStyles,
  style,
  xl,
  md,
  sm,
  id,
  tag = 'div',
}: Props) => {
  const Tag = `${tag}` as keyof JSX.IntrinsicElements;
  return (
    <Tag
      key={classStyles}
      id={id || classStyles}
      style={style}
      className={cx(classStyles, styles.container, {
        [styles.custom]: classStyles,
        [styles.container_fluid]: fluid,
        [styles.container_fixed]: fixed,
        [styles['container-xl']]: xl,
        [styles['container-md']]: md,
        [styles['container-sm']]: sm,
      })}>
      {children}
    </Tag>
  );
};
export default MDContainer;
