import cx from 'classnames';
import { FormProvider, UseFormReturn } from 'react-hook-form';
import styles from './styles.module.scss';

export interface MDFormProviderProps {
  children: JSX.Element | React.ReactNode;
  methods: UseFormReturn<any>;
  className?: string;
  shadown?: boolean;
}

const MDFormProvider: React.FC<MDFormProviderProps> = ({
  children,
  methods,
  className,
  shadown = true,
}): JSX.Element => {
  const keydown = (event: any) => {
    if (event.keyCode === 13) {
      event.preventDefault();
    }
  };

  return (
    <FormProvider {...methods}>
      <form
        onKeyDown={keydown}
        className={cx(styles.MDFormProvider, className, {
          [styles.shadown]: shadown,
        })}>
        {children}
      </form>
    </FormProvider>
  );
};

export default MDFormProvider;
