import { useAppSelector } from '@store/init';
import styles from './styles.module.scss';

const LCLoading = ({ key = '' }: any) => {
  const { total } = useAppSelector((state) => state);

  const loading = total.loading?.[key] as any;

  return (
    <div className={styles.loading}>
      <p>{loading?.message}</p>
    </div>
  );
};

export default LCLoading;
