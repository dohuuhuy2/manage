import cx from 'classnames';
import { ButtonHTMLAttributes, DetailedHTMLProps } from 'react';
import styles from './styles.module.scss';

export type IButton = {
  hidden?: boolean;
  reasonable?: boolean;
  shadown?: boolean;
  hug?: boolean;
  className?: string;
  isBorder?: boolean;
  setBorder?: {
    width?: number;
    style?: string;
    color?: string;
  };
  color?: string;
  background?: string;
  iconLeft?: any;
  iconRight?: any;
  size?: 'sm' | 'md' | 'lg' | 'xl';
} & DetailedHTMLProps<
  ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>;

const MDButton: React.FC<IButton> = ({
  hidden = false,
  shadown = true,
  value,
  iconLeft,
  iconRight,
  isBorder,
  setBorder,
  color,
  background,
  className,
  hug,
  reasonable,
  ...args
}): JSX.Element => {
  const handleClick = (e: any) => {
    e.stopPropagation();
    args?.onClick?.(e);
  };

  return hidden ? (
    <div />
  ) : (
    <button
      {...args}
      onClick={handleClick}
      type="button"
      style={{
        color,
        background,
        borderColor: setBorder?.color,
        borderWidth: setBorder?.width,
        borderStyle: setBorder?.style,
      }}
      className={cx(
        styles.MDButton,
        {
          [styles.border]: isBorder,
          [styles.hug]: hug,
          [styles.shadown]: shadown,
          [styles.reasonable]: reasonable,
        },
        className,
      )}>
      {iconLeft}
      <span className={styles.value}>{value || args?.children}</span>
      {iconRight}
    </button>
  );
};

export default MDButton;
