import cx from 'classnames';
import { RefObject } from 'react';
import ClearIcon from './assets/icon/Clear';
import SearchIcon from './assets/icon/Search';
import styles from './styles.module.scss';

export type IBoxSearch = {
  readonly ref: RefObject<HTMLInputElement>;
  readonly keySearch: string;
  readonly placeholder: string;
  readonly onClear: () => void;
  readonly onChange: (event: any) => void;
};

const BoxSearch: React.FC<IBoxSearch> = ({
  ref,
  onChange,
  placeholder,
  onClear,
  keySearch,
}): JSX.Element => {
  const onClick = (event: React.MouseEvent<HTMLInputElement, MouseEvent>) => {
    event.stopPropagation();
  };
  return (
    <div className={cx(styles.boxSearch)}>
      <div className={styles.searchControl}>
        <SearchIcon />
        <input
          autoFocus
          {...{
            className: styles.inputSearch,
            ref,
            placeholder,
            onClick,
            onChange,
          }}
        />
        {keySearch && <ClearIcon onClick={onClear} />}
      </div>
    </div>
  );
};
export default BoxSearch;
