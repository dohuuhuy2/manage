import cx from 'classnames';
import { filter, uniqueId } from 'lodash';
import React from 'react';
import styles from './styles.module.scss';

export interface IListData {
  readonly list: any[];
  readonly keySearch: string;
  readonly keyShow: string;
  readonly selectItem: (item: any) => void;
  readonly customIcon?: React.ReactNode;
  readonly multiple: boolean | undefined;
  readonly multi: any;
}

const ListData: React.FC<IListData> = ({
  multiple,
  multi = [],
  list,
  keySearch,
  keyShow,
  selectItem,
  customIcon,
}): JSX.Element => {
  let tempList = [...list];

  if (multiple) {
    tempList = filter(tempList, (ele) => {
      return !multi?.find((item: any) => {
        return item?.[keyShow] === ele?.[keyShow];
      });
    });
  }

  if (keySearch.length) {
    tempList = filter(tempList, (item: any) =>
      item?.[keyShow]?.toLowerCase()?.includes(keySearch?.toLowerCase()),
    );
    if (tempList.length < 1) {
      return (
        <p className={styles.FilterNotFound}>
          <center>
            Không tìm thấy từ khóa <b>“{keySearch}”</b> cần tìm.
            <br /> Vui lòng thử lại.
          </center>
        </p>
      );
    }
  }

  return (
    <React.Fragment>
      {tempList.map((ele, index) => {
        return (
          <React.Fragment key={uniqueId()}>
            <button
              type="button"
              key={index}
              className={cx(styles.itemList)}
              onClick={() => selectItem(ele)}>
              {customIcon}
              {ele?.[keyShow]}
              {ele?.state && (
                <p className={cx(styles.state, styles[ele?.state])}>
                  {ele?.state}
                </p>
              )}
            </button>
          </React.Fragment>
        );
      })}
    </React.Fragment>
  );
};

export default ListData;
