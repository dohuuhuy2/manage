import cx from 'classnames';
import { find, size, uniqueId } from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import BoxSearch from './BoxSearch';
import ListData from './ListData';
import ArrowDownIcon from './assets/icon/ArrowDown';
import ArrowUpIcon from './assets/icon/ArrowUp';
import ClearIcon from './assets/icon/Clear';
import styles from './styles.module.scss';

export interface ISearchable<Item> {
  width?: string;
  list: Item[];
  title: string | React.ReactNode;
  switchSearch?: boolean;
  txtSearch?: string;
  keyShow: string;
  keyShortLabel?: string;
  onChange?: (item: Item) => void;
  autoClose?: boolean;
  defaultValue?: string;
  classWrapper?: string;
  classImpact?: string;
  extra?: React.ReactNode;
  iconLeft?: React.ReactNode;
  iconRight?: React.ReactNode;
  customIcon?: React.ReactNode;
  multiple?: boolean;
  multipleSeleted?: any[];
  multipleKey?: string;
  selectd?: any;
  switchMain?: boolean;
}

const MDSearchable: React.FC<ISearchable<any>> = ({
  list = [],
  title = 'Chọn ...',
  switchSearch = false,
  txtSearch = 'Tìm nhanh ...',
  keyShow,
  onChange,
  autoClose = false,
  defaultValue,
  classWrapper,
  classImpact,
  extra,
  iconLeft,
  iconRight,
  keyShortLabel,
  customIcon,
  multiple,
  width,
  multipleSeleted = [],
  selectd,
  switchMain = false,
}): JSX.Element => {
  const refBox = useRef<HTMLInputElement>(null);

  const refSearch = useRef<HTMLInputElement>(null);
  const [openList, setOpenList] = useState(false);
  const [keySearch, setKeySearch] = useState('');
  const [select, setselect] = useState<any>(selectd);
  const [multi, setMulti] = useState<any>(multipleSeleted || []);
  const [selectMain, setSelectMain] = useState<any>(null);

  useEffect(() => {
    if (defaultValue) {
      const item = find(list, { value: defaultValue });
      if (item) {
        setselect(item);
      }
    }
  }, [defaultValue, list]);

  useEffect(() => {
    document.addEventListener('mousedown', closeList);
  }, []);

  useEffect(() => {
    if (multiple) {
      if (multi) {
        onChange?.(multi);
      }
    }
  }, [multi, multiple, onChange]);

  const toggleList = () => {
    setOpenList(!openList);
    setKeySearch('');
  };

  const closeList = (event: any) => {
    if (refBox.current && !refBox.current.contains(event.target)) {
      setOpenList(false);
      setKeySearch('');
      // onChange?.(null);
    }
  };

  const selectItem = (item: any) => {
    if (multiple) {
      setMulti((prev: any) =>
        prev?.includes(item) ? [...prev] : [...prev, item],
      );
    } else {
      item && onChange?.(item);
      setselect(item);
    }
    toggleList();
  };

  const onChangSearch = (event: any) => {
    setKeySearch(event.target.value.toLowerCase());
  };

  const clearSearch = () => {
    setKeySearch('');
    if (refSearch.current) {
      refSearch.current.value = '';
    }
  };

  const onClearSeleted = (item: any) => {
    const arrDel = multi.filter(
      (ele: any) => ele?.[keyShow] !== item?.[keyShow],
    );
    setMulti(arrDel);
  };

  const onChangeMain = ({ item }: any) => {
    const data = multi.map((ele: any) => {
      return Object.assign({}, ele, {
        main: item.slug === ele.slug,
      });
    });
    onChange?.(data);
    setSelectMain(item);
  };

  return (
    <>
      <div
        ref={refBox}
        style={{ width }}
        className={cx(styles.wrapDropdownList, classWrapper)}>
        <div className={cx(styles.wrapImpact, classImpact)}>
          <button
            type="button"
            className={cx(styles.btnImpact, {
              [styles.isFocus]: openList,
            })}
            onClick={toggleList}>
            <>
              {iconLeft}
              <div className={styles.labelImpact}>
                {select?.[keyShortLabel || keyShow] || <span>{title}</span>}
              </div>
              {iconRight || (
                <span className={styles.iconToggleImpact}>
                  {openList ? <ArrowUpIcon /> : <ArrowDownIcon />}
                </span>
              )}
            </>
          </button>
          {extra}
        </div>

        {openList && (
          <>
            <div
              className={cx(styles.wrapContent)}
              onClick={(event) => event.stopPropagation()}
              onMouseLeave={autoClose ? closeList : undefined}>
              {size(list) < 1 ? (
                <div className={styles.NotFound}>
                  Vui lòng kiểm tra dữ liệu !
                </div>
              ) : (
                <>
                  {switchSearch && (
                    <BoxSearch
                      {...({
                        ref: refSearch,
                        keySearch,
                        placeholder: txtSearch,
                        onClear: clearSearch,
                        onChange: onChangSearch,
                      } as const)}
                    />
                  )}

                  <div className={cx(styles.content)}>
                    <ListData
                      {...({
                        multiple,
                        multi,
                        list,
                        keySearch,
                        keyShow,
                        selectItem,
                        customIcon,
                      } as const)}
                    />
                  </div>
                </>
              )}
            </div>
          </>
        )}

        {multiple && size(multi) > 0 && (
          <div className={styles.boxMultiselectd}>
            {multi?.map((ele: any) => {
              const isChecked = selectMain?.slug
                ? selectMain?.slug === ele.slug
                : ele.main;

              return (
                <div key={uniqueId()} className={styles.itemSeleted}>
                  <div className={styles.content}>
                    <p className={styles.txt}>{ele?.[keyShow]}</p>
                    {ele?.state && (
                      <p className={cx(styles.state, styles[ele?.state])}>
                        {ele?.state}
                      </p>
                    )}
                    <ClearIcon onClick={() => onClearSeleted(ele)} />
                  </div>
                  {switchMain && (
                    <div
                      className={cx(styles.mainElement, {
                        [styles.isChecked]: isChecked,
                      })}>
                      <input
                        type={'radio'}
                        name="main"
                        value={ele.slug}
                        checked={isChecked}
                        onChange={(event) => onChangeMain({ event, item: ele })}
                      />
                      main
                    </div>
                  )}
                </div>
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default MDSearchable;
