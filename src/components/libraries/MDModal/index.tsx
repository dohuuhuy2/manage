import cx from 'classnames';
import React, { useLayoutEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import styles from './styles.module.scss';

export type MDModalProps = {
  open: boolean;
  onToggle: () => void;
  overlay?: boolean;
  classFlat?: string;
  classModal?: string;
  children?: JSX.Element | any;
  flat?: boolean;
};

const MDModal: React.FC<MDModalProps> = ({ ...props }): JSX.Element => {
  const {
    open,
    overlay = true,
    classFlat,
    classModal,
    onToggle,
    children,
    flat = true,
  } = props;

  const [wrapperElement, setWrapperElement] = useState<any>();

  useLayoutEffect(() => {
    const element = document.querySelector('#MD_modal');
    setWrapperElement(element);
  }, []);

  const onStopPrpg = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event.stopPropagation();
  };

  return (
    <>
      {wrapperElement &&
        open &&
        ReactDOM.createPortal(
          <>
            <div
              onClick={flat ? onToggle : undefined}
              className={cx(styles.flatModal, {
                classFlat,
                [styles.overlay]: overlay,
              })}>
              <div
                className={cx(styles.modal, classModal)}
                onClick={onStopPrpg}>
                <div className={styles.content}>{children}</div>
              </div>
            </div>
          </>,
          wrapperElement,
        )}
    </>
  );
};

export default MDModal;
