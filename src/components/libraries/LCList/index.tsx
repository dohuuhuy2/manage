import { isArray } from 'lodash';
import React from 'react';
import ItemList from './common/Item';
import { PropsList } from './interface';

const LCList = ({
  as: Tag = 'ul',
  asChil: TagChil = 'li',
  data = [],
  children,
  renderItem,
}: PropsList) => {
  return (
    <Tag>
      {isArray(data) &&
        data?.map((value, index) => {
          const props = {
            index,
            value,
            TagChil,
          } as const;
          return (
            <React.Fragment key={index}>
              {renderItem?.({ ...props }) || children || (
                <ItemList {...props} />
              )}
            </React.Fragment>
          );
        })}
    </Tag>
  );
};

export default LCList;
