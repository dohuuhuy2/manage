import { PropsItem } from '../../interface';

const ItemList = ({ value, TagChil = 'li', children }: PropsItem) => {
  return <TagChil>{children || value}</TagChil>;
};

export default ItemList;
