import React from 'react';

export interface PropsList {
  as?: keyof JSX.IntrinsicElements;
  asChil?: keyof JSX.IntrinsicElements;
  data: any[];
  children?: React.ReactNode;
  renderItem?: (props: renderItemProps) => JSX.Element;
}

export interface PropsItem {
  value?: any;
  TagChil?: PropsList['asChil'];
  children?: PropsList['children'];
}
type renderItemProps = {
  readonly index: number;
  readonly value: any;
  readonly TagChil: keyof JSX.IntrinsicElements;
};
