import cx from 'classnames';
import { isEmpty, isEqual } from 'lodash';
import React, { HTMLInputTypeAttribute } from 'react';
import { Controller, RegisterOptions, useFormContext } from 'react-hook-form';
import styles from './styles.module.scss';

export type IFormControl = {
  name?: string;
  isLabel?: boolean;
  direction?: 'hor' | 'ver';
  textArea?: boolean;
  label?: string;
  isSup?: boolean;
  placeholder?: string;
  customInput?: JSX.Element;
  type?: HTMLInputTypeAttribute;
  rules?: Omit<
    RegisterOptions,
    'valueAsNumber' | 'valueAsDate' | 'setValueAs' | 'disabled'
  >;
  fullWidth?: boolean;
} & React.InputHTMLAttributes<any> &
  React.TextareaHTMLAttributes<any>;

const MDFormControl: React.FC<IFormControl> = ({
  ...props
}: any): JSX.Element => {
  const {
    textArea,
    label,
    isSup,
    placeholder,
    customInput,
    name = label || '',
    direction = 'ver',
    isLabel = true,
    rules,
    fullWidth,
    onChange = undefined,
    type,
  } = props;

  const TextField: any = textArea ? 'textarea' : 'input';

  const { control } = useFormContext();

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field, fieldState: { error } }) => {
        const pLabel = label || name;
        return (
          <div
            className={cx(styles.formControl, {
              [styles.fullWidth]: fullWidth,
            })}>
            <div className={cx(styles.wrap, styles[direction])}>
              {isLabel && (
                <label className={styles.formLabel}>
                  {pLabel} {isSup && <sup>*</sup>}
                </label>
              )}
              {customInput ? (
                React.cloneElement(customInput, { ...props })
              ) : (
                <div className={styles.formInput}>
                  <TextField
                    {...(!isEqual(type, 'file') && field)}
                    {...Object.assign({
                      type,
                      className: cx(styles.input, {
                        [styles.textArea]: textArea,
                      }),
                      placeholder: placeholder || `Nhập ${pLabel}`,
                      rows: textArea ? props.rows || 5 : undefined,
                      onChange: (event: any) => {
                        if (isEqual(type, 'file')) {
                          field.onChange(event.target.files);
                        } else {
                          field.onChange(event);
                        }
                        onChange?.(event);
                      },
                    })}
                  />
                </div>
              )}
            </div>
            {name && (
              <div className={styles.formValid}>
                <span
                  className={cx(
                    isEmpty(error?.message)
                      ? styles.hiddenError
                      : styles.message,
                  )}>
                  {error?.message as any}
                </span>
              </div>
            )}
          </div>
        );
      }}
    />
  );
};

export default MDFormControl;
