import { IconProps } from '@assets/icons/@interface';
import cx from 'classnames';
import React from 'react';
import styles from './styles.module.scss';

export type BoxIconProps = {
  children: any;
  setting?: IconProps;
  className?: string;
  onClick?: any;
};

const BoxIcon: React.FC<BoxIconProps> = ({
  children,
  setting = {},
  className,
  onClick = undefined,
}) => {
  const { width, height, size = 24, ...args } = setting;

  const props = {
    width: width || size,
    height: height || width || size,
    ...args,
  };

  return (
    <span className={cx(styles.BoxIcon, className)} onClick={onClick}>
      {React.cloneElement(children, props)}
    </span>
  );
};

export default BoxIcon;
