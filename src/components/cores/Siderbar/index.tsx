import OpenIcon from '@assets/icons/OpenIcon';
import WidenIcon from '@assets/icons/WidenIcon';
import { MDButton } from '@components/libraries';
import cx from 'classnames';
import { useState } from 'react';
import MenuBar from '../MenuBar';
import styles from './styles.module.scss';

const SiderBar = () => {
  const [widen, setWiden] = useState(false);

  const toggleWiden = () => {
    setWiden(!widen);
  };

  return (
    <div className={cx(styles.colSiderbar, { [styles.widen]: widen })}>
      <MenuBar {...{ widen }} />
      <MDButton onClick={toggleWiden} className={styles.btnWiden}>
        {widen ? (
          <OpenIcon width={24} />
        ) : (
          <>
            <WidenIcon width={24} /> close
          </>
        )}
      </MDButton>
    </div>
  );
};

export default SiderBar;
