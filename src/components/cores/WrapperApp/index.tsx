import { serviceWorker } from '@process/utils/serviceWorker';
import { useAppSelector } from '@store/init';
import { actions } from '@store/reducer';
import React from 'react';
import { useDispatch } from 'react-redux';

const WrapperApp = ({ children }: { children: any }): JSX.Element => {
  const dispatch = useDispatch();

  const { partner } = useAppSelector((state) => state);

  React.useEffect(() => {
    serviceWorker();
  }, []);

  React.useEffect(() => {
    dispatch(actions.requestPartners({}));
    dispatch(actions.requestMenuSiderbar());
  }, [dispatch]);

  React.useEffect(() => {
    if (!partner.detail?.partnerId) {
      dispatch(actions.requestMenuSiderbar());
    }
  }, [dispatch, partner.detail?.partnerId]);

  return <React.Fragment>{children}</React.Fragment>;
};

export default WrapperApp;
