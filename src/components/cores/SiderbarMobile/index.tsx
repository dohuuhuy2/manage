import cx from 'classnames';
import MenuBar from '../MenuBar';
import styles from './styles.module.scss';

type SiderbarMobile = {
  open: boolean;
  onToggle?: () => void;
  onClose: () => void;
};

const SiderbarMobile: React.FC<SiderbarMobile> = ({
  open,
  onClose,
}): JSX.Element => {
  return (
    <div className={cx(open ? styles.SiderbarMobile : styles.widen)}>
      <MenuBar onClose={onClose} />
    </div>
  );
};

export default SiderbarMobile;
