import LogoutIcon from '@assets/icons/LogoutIcon';
import MenuIcon from '@assets/icons/MenuIcon';
import UserIcon from '@assets/icons/UserIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import { useAppSelector } from '@store/init';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import SiderbarMobile from '../SiderbarMobile';
import styles from './styles.module.scss';
import { actions } from '@store/reducer';

const Header = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { user } = useAppSelector((state) => state);
  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = () => {
    setIsOpen(!isOpen);
  };

  const closeOpen = () => {
    setIsOpen(false);
  };

  const isUser = user?.info?.username;

  const onActSign = () => {
    if (isUser) {
      dispatch(actions.setInfo({}));
    } else {
      router.push('/auth/login', undefined);
    }
  };

  return (
    <>
      <div className={styles.navbar}>
        <div className={styles.branch}>
          <>
            <a role="button" className={styles.menu} onClick={toggleOpen}>
              <BoxIcon>
                <MenuIcon />
              </BoxIcon>
            </a>
          </>

          <Link href="/">
            <p className={styles.welcome}>
              <BoxIcon setting={{ size: 32 }}>
                <UserIcon />
              </BoxIcon>
              Xin chào {user?.info?.username}
            </p>
          </Link>
        </div>

        <div className={styles.user}>
          <MDButton
            onClick={onActSign}
            className={styles.btnLog}
            iconLeft={
              <BoxIcon>
                <LogoutIcon />
              </BoxIcon>
            }>
            out
          </MDButton>
        </div>
      </div>
      <SiderbarMobile
        {...({
          open: isOpen,
          onToggle: toggleOpen,
          onClose: closeOpen,
        } as const)}
      />
    </>
  );
};

export default Header;
