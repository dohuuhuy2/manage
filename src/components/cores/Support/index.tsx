import ResetIcon from '@assets/icons/ResetIcon';
import SettingIcon from '@assets/icons/SettingIcon';
import { BoxIcon, MDButton } from '@components/libraries';
import { persistor } from '@store/init';
import { useRouter } from 'next/router';
import { useState } from 'react';
import styles from './styles.module.scss';

const Support = () => {
  const router = useRouter();

  const [openBox, setopenBox] = useState(false);

  const onResetDataStores = () => {
    persistor.purge();
    router.reload();
  };

  const toggleStateBox = () => {
    setopenBox(!openBox);
  };

  return (
    <div className={styles.wrapperSetting}>
      {openBox && (
        <div className={styles.box}>
          <p>Thông tin cài đặt</p>
          <ul className={styles.listSetting}>
            <li className={styles.item}>
              Bạn muốn xóa toàn bộ dữ liệu và cache:{' '}
              <MDButton
                hug
                onClick={onResetDataStores}
                background="white"
                shadown={false}>
                <BoxIcon setting={{ width: 24 }}>
                  <ResetIcon />
                </BoxIcon>
              </MDButton>
            </li>
          </ul>
        </div>
      )}
      <MDButton hug onClick={toggleStateBox}>
        <SettingIcon width={24} height={24} />
      </MDButton>
    </div>
  );
};

export default Support;
