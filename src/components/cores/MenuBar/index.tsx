import cx from 'classnames';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './styles.module.scss';

export type MenuBarProps = {
  onClose?: () => void;
  widen?: boolean;
};

const MenuBar: React.FC<MenuBarProps> = ({ onClose, widen }): JSX.Element => {
  const router = useRouter();

  return (
    <>
      <nav className={cx(styles.listSidebar, { [styles.widen]: widen })}>
        {mocksMenu?.map((ele: any, key = 0) => {
          const activeItem = router.asPath.includes(ele.slug);
          // const showByRole =
          //   user.info?.role === 'admin' ? true : user.info?.role === ele.role;

          return (
            <Link
              href={ele?.slug}
              as={undefined}
              key={key}
              onClick={onClose}
              className={cx(styles.item, {
                [styles.activeItem]: activeItem,
              })}>
              <span>✤</span> <span className={styles.title}>{ele?.title}</span>
              <span className={styles.short}>{ele?.short}</span>
            </Link>
          );
        })}
      </nav>
    </>
  );
};

export default MenuBar;

export const mocksMenu = [
  {
    title: 'Quản Lý Website',
    short: 'partners',
    slug: '/partners',
  },
  {
    title: 'Quản lý JSON',
    slug: '/json',
  },
  {
    title: 'Quản lý Image',
    slug: '/media',
  },
  {
    title: 'Quản lý danh mục',
    slug: '/tags',
  },
  {
    title: 'Quản lý bài viết',
    slug: '/posts',
  },
];
