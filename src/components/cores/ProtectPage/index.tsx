import { useAppSelector } from '@store/init';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

const ProtectPage = ({ children }: { children: JSX.Element }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const { user } = useAppSelector((state) => state);

  useEffect(() => {
    if (!user?.info?.username) {
      router.push('/auth/login', undefined);
    }
  }, [dispatch, router, user?.info?.username]);

  return children;
};

export default ProtectPage;
