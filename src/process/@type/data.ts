export const author = {
  fullname: 'Ruby Ahem',
  slug: 'ruby-ahem_80e3c89f-ec46-4860-9306-40229218d533',
  avatar: 'scscs',
  phone: '0583538872',
  zalo: '',
  youtube: '',
  tiktok: '',
  gmail: '',
  state: 'publish',
  _id: '6404d6207076391432836d77',
  updatedAt: '2023-03-05T17:49:20.277Z',
  createdAt: '2023-03-05T17:49:20.277Z',
};

export const refPost = ['1-ngay-bang-48-gio-so-tay-quan-li-thoi-gian-hieu-qua'];

export const tag = {
  slug: 'sach',
  name: 'Sách ',
  title: 'Sách bậy bạ',
  description:
    'Đấy là những tuyên bố trên mạng xã hội Twitter của Sam Altman, đồng sáng lập kiêm CEO OpenAI, nhà phát triển ChatGPT. Anh này cho rằng, tương lai việc con người được những công cụ AI trợ giúp sẽ “cơ bản là tốt” và sẽ diễn ra nhanh.',
  image:
    'https://strapi.sshop.live/uploads/invest_books_mbaandrews_656972d3ab.jpeg',
  posts: refPost,
  state: 'publish',
  _id: '6402ce5439b01d4ddda85b7f',
  updatedAt: '2023-03-05T19:46:13.959Z',
  createdAt: '2023-03-05T19:46:13.959Z',
};

export const tags = [tag];

export const post = {
  _id: '6404f185fc02dfc38197faca',
  pin: false,
  slug: 'nui-tay-ninh-vung-dat-linh-thieng',
  title: 'Núi tây Ninh - Vùng đất linh thiêng',
  description: 'Có những bài học chỉ cuộc đời mới có thể chỉ dạy cho ta.\\n',
  content:
    '1. Dựa vào người khác sẽ không bao giờ có thể khá lên được\\n\\nCon người chỉ có thể cầu cứu chính mình, chỉ có bản thân bạn mới là chỗ dựa vững chãi nhất cho chính mình. Nếu bản thân không tự nỗ lực vươn lên, dựa dẫm, ỷ lại vào người khác sẽ không bao giờ có thể khá lên được.\\n\\n2. Giao thiệp với những người có ích cho bản thân\\n\\nGiao thiệp nhiều với những người có nhân cách trưởng thành, có phẩm chất đạo đức để giúp bạn trưởng thành hơn, vững vàng hơn trong những lúc nguy nan. Học hỏi nhiều từ những người lớn tuổi, bạn sẽ có được rất nhiều kinh nghiệm.\\n![28986af615827ca900c6b69794095d80.jpg.webp](/uploads/28986af615827ca900c6b69794095d80_jpg_59ee4ca577.webp)\\n3. Sống bình thường là khó nhất\\n\\nMôn học khó nhất là bình thường, những người có thể bình thường trước mọi sóng gió là những người có thể làm được tất cả mọi thứ.  \\n\\n4. Linh hoạt mới tự tại\\n\\nNếu như tư tưởng, suy nghĩ của một người không linh hoạt giống như lấy một cái cốc để làm thuyền trong vũng nước, sẽ mãi mãi không bao giờ có thể di chuyển được. Phải đứng cao nhìn rộng, phải linh hoạt thì mới có thể tự do tự tại bay lượn trong thế gian, vũ trụ này.  \\n\\n5. Tận hưởng cô đơn, tiền không mua được\\n\\nKhi đêm khuya vắng lặng, một mình chạy lên đỉnh núi hoặc sa mạc rộng lớn, vô cùng yên tĩnh, không hiểu vì sao lại chảy nước mắt. Không phải đau khổ, cũng không phải vui sướng, mà đó là cảm giác thoải mái của sự yên tĩnh, mọi thứ trong người như được mở ra, đau khổ, phiền não trong lòng bỗng nhiên biến mất. Đó là cảm giác tận hưởng sự cô đơn mà dù có bỏ tiền ra cũng không thể mua được.  \\n![pexels-photo-1766604.webp](/uploads/pexels_photo_1766604_0eb9a31b92.webp)\\n6. Giúp nhiều thành họa\\n\\n\\"Bát gạo nuôi ân, đấu gạo nuôi thù\\". Tặng người một đấu gạo là ân nhân, tặng người cả gánh gạo là kẻ thù. Giúp đỡ bạn bè, cứu tế bạn bè một chút những lúc khó khăn, họ sẽ mãi cảm kích bạn. Nhưng nếu giúp nhiều quá, họ sẽ không bao giờ thấy đủ.   \\n\\n7. Nén giận\\n\\nNổi cáu và nóng giận không những tổn thương người khác mà còn tổn hại đến mình. Nén giận là một môn nghệ thuật và người biết nén giận cũng là một nghệ sĩ. Tuyệt đối không được tùy tiện nổi nóng, học cách nén giận, đôi khi sẽ giúp bạn tránh khỏi những vạ miệng không đáng có.\\n\\n8. Tham nhiều thì thương nhiều\\n![invest_books_mbaandrews.jpeg](/uploads/invest_books_mbaandrews_656972d3ab.jpeg)',
  authors: author,
  keywords: [],
  image:
    'https://strapi.sshop.live/uploads/79d4182818035ee5c9f7109e76730835_png_1418991a94.webp',
  tags: tags,
  state: 'publish',
  createdAt: '2023-03-05T19:46:13.960Z',
  updatedAt: '2023-03-06T11:28:15.629Z',
  view: 1,
};

export const posts = {
  totalData: 22,
  totalPage: 3,
  limit: 10,
  list: [post],
};

export const SEOPage = {
  key: 'quy-trinh',
  title: 'Quy trình',
  description: 'Quy trình medpro',
  canonical: 'https://medpro.vn',
  openGraph: {
    type: 'website',
    url: 'https://medpro.vn',
    title: 'Quy trình',
    description: 'Quy trình medpro',
    images: [
      {
        url: 'https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg',
        width: 800,
        height: 600,
        alt: 'giới thiệu',
      },
    ],
    site_name: 'Trang Quy trình',
  },
};

export const listSEOPage = [SEOPage];
