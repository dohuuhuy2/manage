import { listSEOPage, post, posts, refPost, SEOPage, tag, tags } from './data';

export type refPost = typeof refPost;
export type PostType = typeof post;
export type ListPostType = typeof posts;

export type TagType = typeof tag;
export type ListTagType = typeof tags;

export type ListSEOPageType = typeof listSEOPage;
export type SEOPageType = typeof SEOPage;
