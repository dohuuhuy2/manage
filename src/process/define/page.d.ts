/* eslint-disable @typescript-eslint/ban-types */
import { NextPage } from 'next';
import { ComponentType } from 'react';

export type IAppInit<P = any> = NextPage<P> & {
  layout?: ComponentType<any>;
  requireAuth?: boolean;
} & P;

export type IPage<P = {}, IP = P> = NextPage<P, IP> & {
  layout: ComponentType<any>;
  requireAuth?: boolean;
} & {
  [K in keyof P]?: P[K];
};


export type ILayout<P = {}> = NextPage<P, IP> & {
  children: React.ReactNode;
  layout?: ComponentType<any>;
  requireAuth?: boolean;
} & {
  [K in keyof P]?: P[K];
};
