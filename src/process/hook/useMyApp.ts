import { actions } from '@store/reducer';
import React from 'react';
import { useDispatch } from 'react-redux';

const useMyApp = () => {
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(actions.requestListPartner());
  }, [dispatch]);
};

export default useMyApp;
