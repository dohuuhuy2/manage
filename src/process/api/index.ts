import current_env from '@process/envs';
import axios, { AxiosInstance } from 'axios';

const axins: AxiosInstance = axios.create({
  baseURL: current_env.API_BE,
  timeout: 5000,
  headers: {
    accept: '*/*',
    'Content-Type': 'application/json',
  },
});

export default axins;
