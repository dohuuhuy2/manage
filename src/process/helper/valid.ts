import { size } from 'lodash';
import validator from 'validator';
export const valid = {
  required: (label = '') => {
    return {
      required: {
        value: true,
        message: `${label} không được trống!`,
      },
    };
  },
  fullName: () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (!value) {
            return 'Vui lòng nhập họ và tên!';
          }
        },
      },
    };
  },
  birthYear: () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (size(value) !== 4) {
            return 'Vui lòng nhập đúng năm sinh! Ví dụ: 1998';
          }
        },
      },
    };
  },
  birthdate: () => {
    return {
      validate: {
        required: (value: any) => {
          if (value) {
            if (value === '__/__/20__') {
              return 'Vui lòng nhập ngày tháng năm !';
            }
          } else {
            return 'Vui lòng nhập ngày tháng năm !';
          }
        },
      },
    };
  },
  mobile: () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (value) {
            if (!validator.isMobilePhone(value, 'vi-VN')) {
              return 'Số điện thoại không đúng!';
            }
          }
        },
      },
    };
  },
  email: () => {
    return {
      validate: {
        pattern: (value: string) => {
          if (!validator.isEmail(value) && value) {
            return 'Email không đúng định dạng!';
          }
        },
      },
    };
  },
  sex: () => {
    return {
      validate: {
        required: (value: any) => {
          if (value === 'default') {
            return 'Vui lòng chọn giới tính!';
          }
        },
      },
    };
  },
  career: () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn nghề nghiệp!';
          if (value === 'default') return 'Vui lòng chọn nghề nghiệp!';
        },
      },
    };
  },
  city: () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn Tỉnh / Thành!';
          if (value === 'default') return 'Vui lòng chọn Tỉnh / Thành!';
        },
      },
    };
  },
  district: () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn Quận / Huyện!';
          if (value === 'default') return 'Vui lòng chọn Quận / Huyện!';
        },
      },
    };
  },
  ward: () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn Phường / Xã!';
          if (value === 'default') return 'Vui lòng chọn Phường / Xã!';
        },
      },
    };
  },
  address: () => {
    return {
      required: {
        value: true,
        message: 'Vui lòng nhập chi tiết Địa chỉ!',
      },
    };
  },
  CMND: () => {
    return {
      validate: {
        pattern: (value: any) => {
          if (value) {
            if (![8, 9, 12].includes(value.length)) {
              return 'Vui lòng nhập đúng thông tin!';
            }
          }
        },
      },
    };
  },
  relativeType: () => {
    return {
      validate: {
        required: (value: any) => {
          if (!value) return 'Vui lòng chọn quan hệ bệnh nhân!';
          if (value === 'default') {
            return 'Vui lòng chọn quan hệ bệnh nhân!';
          }
        },
      },
    };
  },
};

export default valid;
