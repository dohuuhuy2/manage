import { get, isObject } from 'lodash';
import { NextRouter } from 'next/router';
import format from './format';

export const quick = {
  // -----------------
  // -----------start----------
  onCopy: async (txt: string) => {
    if (typeof navigator.clipboard === 'undefined') {
      const textArea = document.createElement('textarea');
      textArea.value = txt;
      textArea.style.position = 'fixed';
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();
      document.execCommand('copy');
      document.body.removeChild(textArea);
      return;
    } else {
      navigator.clipboard?.writeText && navigator.clipboard?.writeText(txt);
    }
  },

  onUpperFirst: (txt: string) => {
    return txt.charAt(0).toUpperCase() + txt.slice(1);
  },

  onLoaderImg: ({ src, width, quality }: any): string => {
    return `${src}?w=${width}&q=${quality || 75}`;
  },

  onScrollById: ({ Id }: { Id: string }) => {
    setTimeout(() => {
      const element = document.getElementById(Id);
      if (element) element.scrollIntoView({ behavior: 'smooth' });
    }, 100);
  },

  scrollToTopId: ({ Id }: { Id: string }) => {
    setTimeout(() => {
      const element = document.getElementById(Id);
      if (element)
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }, 0);
  },

  scrollToBottomId: ({ Id }: { Id: string }) => {
    setTimeout(() => {
      const element = document.getElementById(Id);
      if (element) element.scrollIntoView({ behavior: 'smooth', block: 'end' });
    }, 0);
  },

  onScrollToTop: () => {
    return window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  },

  onScrollToBottom: () => {
    return window.scrollTo(0, document.body.scrollHeight);
  },

  getError: (error: any) => {
    const err = get(error, 'response.data', '');
    return {
      error: true,
      ...err,
      ...error,
    };
  },

  onPush: (txt: string, router: NextRouter) => () => {
    router.push(txt, undefined);
  },

  randomColor: () => {
    return `#${Math.floor(Math.random() * 0xffffff).toString(16)}`;
  },

  getSegementUrl: (path = '') => {
    if (path.length > 1) return path.replace('/', '');
    else return path;
  },

  getPlatform: () => {
    if (window.innerWidth < 768.98) {
      return 'mobile';
    } else {
      return 'web';
    }
  },

  getTypeOS: (userAgent: any) => {
    if (/windows phone/i.test(userAgent)) {
      return 'Windows Phone';
    }
    if (/android/i.test(userAgent)) {
      return 'android';
    }
    if (/iPad|iPhone|iPod/.test(userAgent)) {
      return 'ios';
    }
  },

  slugify: (str: string) => {
    str = str.toLowerCase();
    str = str.trim();
    str = format.formatUTF8(str);
    str = str.replace(/[^\w\s-]/g, '');
    str = str.replace(/[\s_-]+/g, '-');
    str = str.replace(/^-+|-+$/g, '');

    return str;
  },

  isHtml: (str: any) => {
    return /^\s*</.test(str);
  },

  uuidv4: () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (char) => {
      const random = (Math.random() * 16) | 0;
      const value = char === 'x' ? random : (random & 0x3) | 0x8;
      return value.toString(16);
    });
  },
  objectToFormData: (obj: any) => {
    const formData = new FormData();
    if (isObject(obj)) {
      Object.keys(obj).map((key) => {
        return formData.append(key, (obj as any)[key]);
      });
      return formData;
    }
    return formData;
  },
};

export default quick;
