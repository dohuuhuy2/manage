export const specialCharacters = /[`~!@#$%^&*()_|+\-=?;:'",.\\{\\}\\[\]\\\\/]/g;

export const format = {
  formatUTF8: (value: any) => {
    value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    value = value.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    value = value.replace(/o|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    value = value.replace(/đ|d/g, 'd');
    value = value.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // Huyền sắc hỏi ngã nặng
    value = value.replace(/\u02C6|\u0306|\u031B/g, ''); // Â, Ê, Ă, Ơ, Ư
    return value;
  },
  name: (value: any) => {
    value = value.replace(specialCharacters, '');
    value = value.replace(/^\s+|\s+$/g, ' ');
    value = value.toLowerCase();
    value = format.formatUTF8(value);
    return value;
  },
  value: (value: any) => {
    value = value.replace(specialCharacters, '');
    value = value.replace(/\s+|\s+$/g, ' ');
    return value;
  },
  coverPhone: (str = '') => {
    if (str) {
      const result = str.split('');
      result.splice(3, 4, '*', '*', '*', '*');
      const rs = result.join('');
      return rs;
    } else {
      return str;
    }
  },
  money: (text = 0) => {
    return text.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
  },
  money2: (str: any) => {
    const parts = str.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    return `${parts.join('.')} ` + 'đ';
  },
  isHtml: (str: any) => {
    return /<[a-z][\s\S]*>/i.test(str);
  },
};
export default format;
