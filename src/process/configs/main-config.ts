import { ENV } from '@process/envs';
import { storage } from '@store/reducer/storage';

const isClientSide = typeof window !== 'undefined';
const IS_DEV = ENV === 'develop';

export const mainConfig = {
  reduxPersist: {
    key: 'root',
    storage,
    version: 0.4,
    enabled: true,
  },
  isDevEnv: true || !IS_DEV,
  isClientSide,
};

export default mainConfig;
