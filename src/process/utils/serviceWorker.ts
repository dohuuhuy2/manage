import current_env from '@process/envs';
import { clog } from './clog';
import { listen } from './func';

export const serviceWorker = () => {
  clog({
    name: 'listen',
    child: {
      ...listen(),
      ...current_env,
    },
    type: 'info',
  });

  if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/sw.js').then(
        (registration) => {
          clog({
            name: 'Service Worker registration successful with scope',
            child: registration.scope,
            type: 'info',
          });
        },
        (err) => {
          console.info('Service Worker registration failed: ', err);
        },
      );
    });
  }
};
