import { get } from 'lodash';

const size = 'font-size: 0.813rem;';

interface Log {
  name: string;
  child: any;
  type: 'error' | 'warning' | 'info' | 'success' | 0 | 1 | 2 | 3;
}

export const clog = ({ name, child, type }: Log) => {
  const err = get(child, 'response.data', '');

  switch (type) {
    case 0:
    case 'error':
      if (err) {
        console.info(
          `%cLỗi - ${name} :>> `,
          `color: #dc3545; ${size}`,
          `
          - StatusCode :>>  ${err?.statusCode || 404},
          - Message :>> ${err.message},
        `,
        );
        console.info(
          `- %cChi tiết lỗi ${name} :>> `,
          `color: #dc3545; ${size}`,
          err,
        );
      } else {
        console.info(`%cLỗi - ${name} :>> `, `color: #dc3545; ${size}`, child);
      }

      break;
    case 1:
    case 'warning':
      return console.info(
        `%cCảnh báo - ${name} :>> `,
        `color: #ffc107; ${size}`,
        child,
      );

    case 2:
    case 'info':
      return console.info(
        `%cThông tin - ${name} :>> `,
        `color: #007bff; ${size}`,
        child,
      );

    case 3:
    case 'success':
      return console.info(
        `%cThành công - ${name} :>> `,
        `color:#28a745 ; ${size}`,
        child,
      );

    default:
      return console.info(
        `%cKết quả - ${name} :>> `,
        `color:#6f42c1 ; ${size}`,
        child,
      );
  }
};
