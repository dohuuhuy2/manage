export const randomColor = () => {
  return colors2[Math.floor(Math.random() * colors2.length)];
};

export const colors2 = [
  '#f4ed83',
  '#f3f29f',
  '#f6f1cb',
  '#ffce71',
  '#ffc27f',
  '#fecdbf',
  '#febbcc',
  '#ffe9fe',
  '#f4bad3',
  '#ffd9ee',
  '#f49fe3',
  '#e99ff4',
  '#efd1f3',
  '#e0d0f4',
  '#a4f49d',
  '#9ff4bd',
  '#cdf59f',
  '#def59d',
  '#d2f4d1',
  '#e1f8fe',
  '#9bf6ef',
  '#9eeef5',
  '#9ddef4',
];
export const colors = [
  '#fee92c',
  '#f4ed83',
  '#f3f29f',
  '#f6f1cb',
  '#ffce71',
  '#ffbc54',
  '#ffc27f',
  '#fecdbf',
  '#febbcc',
  '#ff70b8',
  '#ffe9fe',
  '#f4bad3',
  '#ffd9ee',
  '#f49fe3',
  '#e99ff4',
  '#dca1f5',
  '#d06dfe',
  '#efd1f3',
  '#e0d0f4',
  '#9fb1f3',
  '#a4f49d',
  '#9ff4bd',
  '#cdf59f',
  '#def59d',
  '#d2f4d1',
  '#e1f8fe',
  '#9bf6ef',
  '#9eeef5',
  '#9ddef4',
  '#9ec2f4',
];
