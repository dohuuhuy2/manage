import { NextRouter } from 'next/router';
import { toast } from 'react-toastify';

export const processUrl = ({
  router,
  position = 1,
}: {
  router: NextRouter;
  position?: number;
}) => {
  if (router) {
    const { pathname } = router;
    const getSegment = pathname.split('/');
    return getSegment[position];
  }
  return;
};

export const listen = () => {
  const IP_LAN = process.env['IP_LAN'];
  const PORT = process.env['PORT'];
  const ENV = process.env['ENV'];
  const running = `http://${IP_LAN}:${PORT}`;
  const url = (port = 80) => `http://${IP_LAN}:${port}`;

  return {
    IP_LAN,
    PORT,
    ENV,
    running,
    url,
  };
};

export type OptionPartner = { list: any[] };

export const onCoppy = async (txt: string) => {
  toast.success('Copy success !', {
    position: 'top-right',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: 'light',
  });

  if (typeof navigator.clipboard === 'undefined') {
    const textArea = document.createElement('textarea');
    textArea.value = txt;
    textArea.style.position = 'fixed';
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    document.execCommand('copy');

    document.body.removeChild(textArea);
    return;
  } else {
    navigator.clipboard?.writeText(txt);
  }
};

export const upperFirst = (txt: string) => {
  return txt.charAt(0).toUpperCase() + txt.slice(1);
};

export const myLoader = ({ src, width, quality }: any): string => {
  return `${src}?w=${width}&q=${quality || 75}`;
};

export const scrollById = ({ Id }: { Id: string }) => {
  setTimeout(() => {
    const element = document.getElementById(Id);
    if (element) element.scrollIntoView({ behavior: 'smooth' });
  }, 500);
};

export const isJson = (data = '') => {
  if (!(data && typeof data === 'string')) {
    return false;
  }

  try {
    JSON.parse(data);
    return true;
  } catch (error) {
    console.error('error :>> ', error);
    return false;
  }
};

export const toBlob = async (data: any) => {
  return await fetch(data).then((res) => res.blob());
};
