import { VERSION, VERSION_CONTENT } from '@process/configs/version';
import current_env from '@process/envs';
import moment from 'moment-timezone';
import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();

const info_app = {
  'Thời điểm build source :>>': publicRuntimeConfig.modifiedDate,
  'Build gần nhất vào lúc :>>': moment(
    publicRuntimeConfig.modifiedDate,
    'DD/MM/YYYY HH:mm:ss',
  ).from(moment()),
  version: VERSION,
  VERSION_CONTENT,
  env: current_env,
};
export default info_app;
