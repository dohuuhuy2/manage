import envs, { KeyEnvs } from './env.export';

declare let process: {
  env: {
    ENV: KeyEnvs;
  };
};

export const ENV: KeyEnvs = process.env.ENV || 'testing';
const current_env = envs[ENV];

export default current_env;
