import * as develop from './env.file/develop';
import * as production from './env.file/production';
import * as testing from './env.file/testing';

const envs = {
  develop,
  testing,
  production,
};

export type KeyEnvs = keyof typeof envs;

export default envs;
