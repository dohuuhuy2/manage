import { CSSProperties } from 'react';

export type IconProps = {
  fill?: CSSProperties['color'];
  size?: number;
  height?: number;
  width?: number;
  label?: string;
  stroke?: CSSProperties['color'];
};
