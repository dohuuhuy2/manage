import React from 'react';
import { IconProps } from './@interface';

export default ({ ...props }: IconProps): JSX.Element => {
  return React.cloneElement(
    <svg viewBox="0 0 24 24" fill="none">
      <path
        stroke="#000000"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M22 12c0 5.52-4.48 10-10 10s-8.89-5.56-8.89-5.56m0 0h4.52m-4.52 0v5M2 12C2 6.48 6.44 2 12 2c6.67 0 10 5.56 10 5.56m0 0v-5m0 5h-4.44"
      />
    </svg>,
    props,
  );
};
