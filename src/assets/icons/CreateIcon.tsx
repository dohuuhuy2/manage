import React from 'react';
import { IconProps } from './@interface';

export default ({ fill, ...props }: IconProps): JSX.Element => {
  return React.cloneElement(
    <svg
      {...props}
      width="1em"
      height="1em"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg">
      <path
        {...props}
        d="M24 13.604a.3.3 0 01-.3.3h-9.795V23.7a.3.3 0 01-.3.3h-3.21a.3.3 0 01-.3-.3v-9.795H.3a.3.3 0 01-.3-.3v-3.21a.3.3 0 01.3-.3h9.795V.3a.3.3 0 01.3-.3h3.21a.3.3 0 01.3.3v9.795H23.7a.3.3 0 01.3.3v3.21z"
        fill={fill}
      />
    </svg>,
    props,
  );
};
