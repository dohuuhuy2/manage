import React from 'react';
import { IconProps } from './@interface';

export default ({ ...props }: IconProps): JSX.Element => {
  return React.cloneElement(<svg />, props);
};
