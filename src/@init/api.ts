import { listen } from '@process/utils/func';

export const urlBE = {
  DEVELOP: listen().url(3019),
  TESTING: 'https://api.sshop.live',
  PRODUCTION: 'https://api.sshop.live',
};
