export const urlPage = {
  home: '/',
  quanLyJSON: '/json',

  quanLyHinhAnh: '/image',
  demo: '/demo',

  quanlyDanhMuc: '/tags',
  chiTietDanhMuc: '/tags/form',

  quanLyBaiViet: '/posts',
  chiTietBaiViet: '/posts/form',

  quanLyDoiTac: '/partners',
  chiTietDoiTac: '/partners/form',
  chonDoiTac: '/partners/choose-partners',

  quanLyTacGia: '/authors',
  chiTietTacGia: '/authors/form',

  quanLySEO: '/SEO',
  chiTietSEO: '/SEO/form',

  quanLyKeywords: '/keywords',
  chiTietKeywords: '/keywords/form',
};
